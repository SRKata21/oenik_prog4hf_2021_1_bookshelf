var class_m_v_c_web_app_1_1_models_1_1_reader_m_v_c =
[
    [ "Address", "class_m_v_c_web_app_1_1_models_1_1_reader_m_v_c.html#a69531e7d9962d359428975215aedb139", null ],
    [ "BirthDate", "class_m_v_c_web_app_1_1_models_1_1_reader_m_v_c.html#adb5c2d0584bd65f28ec8657be3deeedf", null ],
    [ "BirthPlace", "class_m_v_c_web_app_1_1_models_1_1_reader_m_v_c.html#afe027439b26c41b3bb9be686ba3faea3", null ],
    [ "Email", "class_m_v_c_web_app_1_1_models_1_1_reader_m_v_c.html#a654ec8fb77dc4eb367a3eff57957e961", null ],
    [ "EnterDate", "class_m_v_c_web_app_1_1_models_1_1_reader_m_v_c.html#a921685f8c834902c522a356e7786e19c", null ],
    [ "MotherName", "class_m_v_c_web_app_1_1_models_1_1_reader_m_v_c.html#a4fefe350836e848c2b9e069db3aee2c9", null ],
    [ "Name", "class_m_v_c_web_app_1_1_models_1_1_reader_m_v_c.html#a25702ddb419c5a0e6107f1bfe2794c93", null ],
    [ "PersonId", "class_m_v_c_web_app_1_1_models_1_1_reader_m_v_c.html#a2b7f0e5484d788a308487f197a2e60d8", null ]
];