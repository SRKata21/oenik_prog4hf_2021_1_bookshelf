var namespace_book_shelf_1_1_wpf_app =
[
    [ "BL", "namespace_book_shelf_1_1_wpf_app_1_1_b_l.html", "namespace_book_shelf_1_1_wpf_app_1_1_b_l" ],
    [ "Data", "namespace_book_shelf_1_1_wpf_app_1_1_data.html", "namespace_book_shelf_1_1_wpf_app_1_1_data" ],
    [ "UI", "namespace_book_shelf_1_1_wpf_app_1_1_u_i.html", "namespace_book_shelf_1_1_wpf_app_1_1_u_i" ],
    [ "VM", "namespace_book_shelf_1_1_wpf_app_1_1_v_m.html", "namespace_book_shelf_1_1_wpf_app_1_1_v_m" ],
    [ "App", "class_book_shelf_1_1_wpf_app_1_1_app.html", "class_book_shelf_1_1_wpf_app_1_1_app" ],
    [ "MainWindow", "class_book_shelf_1_1_wpf_app_1_1_main_window.html", "class_book_shelf_1_1_wpf_app_1_1_main_window" ]
];