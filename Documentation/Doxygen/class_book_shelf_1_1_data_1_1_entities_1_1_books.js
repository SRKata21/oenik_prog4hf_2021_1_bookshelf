var class_book_shelf_1_1_data_1_1_entities_1_1_books =
[
    [ "Books", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html#a57daf86e932aebcb247c6507eff14b6e", null ],
    [ "Equals", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html#a2ec3c498528804df8ab7b1922cb11a57", null ],
    [ "GetHashCode", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html#aae8711b0dda85a93b18ceb0aec296171", null ],
    [ "ToString", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html#a8272be63d53ff130286d28839abad560", null ],
    [ "AuthorId", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html#af3a832483ff9eb6224c1a7897e2d46fe", null ],
    [ "Authr", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html#adbdbda180f7c4ccf4d1b89ccfb42be31", null ],
    [ "BookId", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html#ac57b1bd42cca27798e4375ba5a8131eb", null ],
    [ "GetDate", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html#a4afc3b357a3186ecdea3382e37f11512", null ],
    [ "Lnd", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html#a445c07bef92caff12d1a16fa1ef5e9a2", null ],
    [ "Pages", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html#a88ca864251efc53975ae81cba7daccc7", null ],
    [ "Place", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html#a971f6cfe79108c0edcd63ac13b26fe24", null ],
    [ "Publisher", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html#af2df60194945cb0f839c4d173cec5a69", null ],
    [ "Title", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html#ac02d31366d5cfb3ae99e6ed864a0a0d3", null ],
    [ "Year", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html#ad7f5ba4b8622a9839a1159c75c6bde29", null ]
];