var class_wpf_client_1_1_main_v_m =
[
    [ "MainVM", "class_wpf_client_1_1_main_v_m.html#af433595581a365e8237d869418d9afc5", null ],
    [ "MainVM", "class_wpf_client_1_1_main_v_m.html#a21196e218d3c30fe3ad82800a51d8f91", null ],
    [ "AddCmd", "class_wpf_client_1_1_main_v_m.html#a17722de5d04f46a4bcc46c70f1f002a1", null ],
    [ "AllReader", "class_wpf_client_1_1_main_v_m.html#a3c8040afd93d940b9398144a62281a76", null ],
    [ "DelCmd", "class_wpf_client_1_1_main_v_m.html#a022cdab537a1a854a1d9f7eb668bfe2d", null ],
    [ "EditorFunc", "class_wpf_client_1_1_main_v_m.html#a9635fcfdb10f23776e9cf061a6e8daf1", null ],
    [ "LoadCmd", "class_wpf_client_1_1_main_v_m.html#ad1714be778289ce3b829196a063ac7e6", null ],
    [ "ModCmd", "class_wpf_client_1_1_main_v_m.html#ad1db540820758c1b86ad86ddee3997a9", null ],
    [ "SelectedReader", "class_wpf_client_1_1_main_v_m.html#a3d3336b4cde86d68d6f1262918402cb7", null ]
];