var namespace_book_shelf_1_1_wpf_app_1_1_b_l =
[
    [ "Factory", "class_book_shelf_1_1_wpf_app_1_1_b_l_1_1_factory.html", "class_book_shelf_1_1_wpf_app_1_1_b_l_1_1_factory" ],
    [ "IEditorService", "interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_editor_service.html", "interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_editor_service" ],
    [ "ILogic", "interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_logic.html", "interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_logic" ],
    [ "LendLogic", "class_book_shelf_1_1_wpf_app_1_1_b_l_1_1_lend_logic.html", "class_book_shelf_1_1_wpf_app_1_1_b_l_1_1_lend_logic" ],
    [ "ReaderLogic", "class_book_shelf_1_1_wpf_app_1_1_b_l_1_1_reader_logic.html", "class_book_shelf_1_1_wpf_app_1_1_b_l_1_1_reader_logic" ]
];