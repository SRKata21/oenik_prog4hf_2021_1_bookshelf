var class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context =
[
    [ "BookShelfDBContext", "class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html#aedd8f9bbc447ffa75d3399f2ac3ff097", null ],
    [ "OnConfiguring", "class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html#acb925368141865408f85164e149162c4", null ],
    [ "OnModelCreating", "class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html#a911f41fec4be3017c368767939124bbd", null ],
    [ "Authors", "class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html#a3485c6552909c27645ee8b04c63ad89c", null ],
    [ "Books", "class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html#a1700ac9e5bd8449ae35f01fad66cbb97", null ],
    [ "Lends", "class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html#ab4b250d3e4497f9093b9eba9b25851c6", null ],
    [ "People", "class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html#a910dafb6aa24e9e241a6d2bccf677b1a", null ]
];