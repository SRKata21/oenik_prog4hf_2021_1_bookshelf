var namespace_book_shelf_1_1_wpf_app_1_1_v_m =
[
    [ "LendEditorViewModel", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_lend_editor_view_model.html", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_lend_editor_view_model" ],
    [ "LibrarianViewModel", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_librarian_view_model.html", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_librarian_view_model" ],
    [ "ReaderEditorViewModel", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_editor_view_model.html", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_editor_view_model" ],
    [ "ReaderViewModel", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_view_model.html", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_view_model" ]
];