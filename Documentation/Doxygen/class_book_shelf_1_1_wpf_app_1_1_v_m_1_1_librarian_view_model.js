var class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_librarian_view_model =
[
    [ "LibrarianViewModel", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_librarian_view_model.html#a426c40606665fdfd03202ef568f47a43", null ],
    [ "LibrarianViewModel", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_librarian_view_model.html#aaa3489f33af2b066a8962fed6c68f12f", null ],
    [ "AllLends", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_librarian_view_model.html#a5332a667f3d2d2cea0cfefd93940961f", null ],
    [ "GiveBComm", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_librarian_view_model.html#a2b875c843c3dd081a8e6018decea23d9", null ],
    [ "NewComm", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_librarian_view_model.html#a78b3d29158395940ddd8753effae4b40", null ],
    [ "SelectedLend", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_librarian_view_model.html#a1db40cfecc72e5ef4d7b1b6c509f221d", null ]
];