var class_book_shelf_1_1_data_1_1_entities_1_1_people =
[
    [ "People", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html#a0ff0235486b7e1a42fcb269ce7fd2b83", null ],
    [ "Equals", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html#a2a4af50f710ae201632316423ffd6c85", null ],
    [ "GetHashCode", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html#a4b46ad2ccfc6337923b40aa89988780a", null ],
    [ "ToString", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html#aed91aada2686a1d057f8de2d0b464020", null ],
    [ "Address", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html#abd0cde8e3f75f2ffacd0109bc66ece7f", null ],
    [ "BirthDate", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html#aef58ea514af63c578f1021308175bf9b", null ],
    [ "BirthPlace", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html#a4edd845864cd7c1690e9974e2c443694", null ],
    [ "Email", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html#aff327141de52a457422e88e77e9acce6", null ],
    [ "EnterDate", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html#a781976f763e74e4ca2189caba7d9b3b2", null ],
    [ "Lnd", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html#a90419befaf4ad068b88643ea983b3c42", null ],
    [ "MotherName", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html#ae59923556086a7df863793f18df3876b", null ],
    [ "Name", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html#a7b37221bca61bc4d4ded11a04e26d571", null ],
    [ "PersonId", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html#a78ead423b82bb24d2394c2a87a179771", null ]
];