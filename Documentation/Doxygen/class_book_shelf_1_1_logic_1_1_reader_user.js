var class_book_shelf_1_1_logic_1_1_reader_user =
[
    [ "ReaderUser", "class_book_shelf_1_1_logic_1_1_reader_user.html#a0fae477c96bb1d12cbf42c321e0c3cd5", null ],
    [ "ChangePersonAddress", "class_book_shelf_1_1_logic_1_1_reader_user.html#a6b9218e6b612ba62e8131fac548470d6", null ],
    [ "ChangePersonEmail", "class_book_shelf_1_1_logic_1_1_reader_user.html#ada6e160e725e99a08504c862d9beaaa5", null ],
    [ "GetAllAuthors", "class_book_shelf_1_1_logic_1_1_reader_user.html#aa70d383f6fdb6d25166bb6c31c61285b", null ],
    [ "GetAllBooks", "class_book_shelf_1_1_logic_1_1_reader_user.html#a1ef35718aa552418ea082b156dcaa368", null ],
    [ "GetBookHungarian", "class_book_shelf_1_1_logic_1_1_reader_user.html#ae0e648a9454a04a1a202e98f6e45309e", null ],
    [ "GetFamousBooks", "class_book_shelf_1_1_logic_1_1_reader_user.html#a257950cbc45aef6b69e60c7880f3ec0b", null ],
    [ "TaskGetBookHungarian", "class_book_shelf_1_1_logic_1_1_reader_user.html#a748c134b62cc45938bcd186fbc3c8cee", null ],
    [ "TaskGetFamBook", "class_book_shelf_1_1_logic_1_1_reader_user.html#a03e666257dc5fd3199a586ea0daab69a", null ]
];