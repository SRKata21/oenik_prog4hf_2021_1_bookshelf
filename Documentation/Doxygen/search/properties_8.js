var searchData=
[
  ['mockauthorrepo_552',['MockAuthorRepo',['../class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#ad6b5c622cb139e42e601633bef77b97b',1,'BookShelf::Logic::Tests::LogicCreator']]],
  ['mockbookrepo_553',['MockBookRepo',['../class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#a47ff680302e2f70dc5b63909bab5a874',1,'BookShelf::Logic::Tests::LogicCreator']]],
  ['mocklendrepo_554',['MockLendRepo',['../class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#aecba52869f40e99b55bd06531b16a9a9',1,'BookShelf::Logic::Tests::LogicCreator']]],
  ['mockpeoplerepo_555',['MockPeopleRepo',['../class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#a32b858e47a7bbf4a7b7b19b47c3ddf15',1,'BookShelf::Logic::Tests::LogicCreator']]],
  ['modcmd_556',['ModCmd',['../class_wpf_client_1_1_main_v_m.html#ad1db540820758c1b86ad86ddee3997a9',1,'WpfClient::MainVM']]],
  ['modifycomm_557',['ModifyComm',['../class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_view_model.html#af1f89ea9cb0d7d3ae6dd5b5a760d2227',1,'BookShelf::WpfApp::VM::ReaderViewModel']]],
  ['mothername_558',['MotherName',['../class_book_shelf_1_1_data_1_1_entities_1_1_people.html#ae59923556086a7df863793f18df3876b',1,'BookShelf.Data.Entities.People.MotherName()'],['../class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w.html#aaf333e25ae2b9cfac5c389cbde0e95fc',1,'BookShelf.WpfApp.Data.ReadersW.MotherName()'],['../class_m_v_c_web_app_1_1_models_1_1_reader_m_v_c.html#a4fefe350836e848c2b9e069db3aee2c9',1,'MVCWebApp.Models.ReaderMVC.MotherName()']]]
];
