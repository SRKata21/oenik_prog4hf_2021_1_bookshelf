var searchData=
[
  ['bl_361',['BL',['../namespace_book_shelf_1_1_wpf_app_1_1_b_l.html',1,'BookShelf::WpfApp']]],
  ['bookshelf_362',['BookShelf',['../namespace_book_shelf.html',1,'']]],
  ['data_363',['Data',['../namespace_book_shelf_1_1_data.html',1,'BookShelf.Data'],['../namespace_book_shelf_1_1_wpf_app_1_1_data.html',1,'BookShelf.WpfApp.Data']]],
  ['entities_364',['Entities',['../namespace_book_shelf_1_1_data_1_1_entities.html',1,'BookShelf::Data']]],
  ['logic_365',['Logic',['../namespace_book_shelf_1_1_logic.html',1,'BookShelf']]],
  ['repository_366',['Repository',['../namespace_book_shelf_1_1_repository.html',1,'BookShelf']]],
  ['tests_367',['Tests',['../namespace_book_shelf_1_1_logic_1_1_tests.html',1,'BookShelf::Logic']]],
  ['ui_368',['UI',['../namespace_book_shelf_1_1_wpf_app_1_1_u_i.html',1,'BookShelf::WpfApp']]],
  ['vm_369',['VM',['../namespace_book_shelf_1_1_wpf_app_1_1_v_m.html',1,'BookShelf::WpfApp']]],
  ['wpfapp_370',['WpfApp',['../namespace_book_shelf_1_1_wpf_app.html',1,'BookShelf']]]
];
