var searchData=
[
  ['birthcountry_22',['BirthCountry',['../class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#a0bf171d87e03a5d789a450d8ba4ddb5c',1,'BookShelf.Data.Entities.Authors.BirthCountry()'],['../class_book_shelf_1_1_wpf_app_1_1_data_1_1_authors_w.html#a14738e608e383c75a15f1979610f1f5e',1,'BookShelf.WpfApp.Data.AuthorsW.BirthCountry()']]],
  ['birthdate_23',['BirthDate',['../class_book_shelf_1_1_data_1_1_entities_1_1_people.html#aef58ea514af63c578f1021308175bf9b',1,'BookShelf.Data.Entities.People.BirthDate()'],['../class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w.html#a5649a29ebb16616cad21c55fe5a47d59',1,'BookShelf.WpfApp.Data.ReadersW.BirthDate()'],['../class_m_v_c_web_app_1_1_models_1_1_reader_m_v_c.html#adb5c2d0584bd65f28ec8657be3deeedf',1,'MVCWebApp.Models.ReaderMVC.BirthDate()']]],
  ['birthname_24',['BirthName',['../class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#a7558e274ec063414cc20df8137f43838',1,'BookShelf.Data.Entities.Authors.BirthName()'],['../class_book_shelf_1_1_wpf_app_1_1_data_1_1_authors_w.html#a790caacdb2ca15803eac2be0c1aa9d62',1,'BookShelf.WpfApp.Data.AuthorsW.BirthName()']]],
  ['birthplace_25',['BirthPlace',['../class_book_shelf_1_1_data_1_1_entities_1_1_people.html#a4edd845864cd7c1690e9974e2c443694',1,'BookShelf.Data.Entities.People.BirthPlace()'],['../class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w.html#afa0b34c80f943e46d4d063690a778b4b',1,'BookShelf.WpfApp.Data.ReadersW.BirthPlace()'],['../class_m_v_c_web_app_1_1_models_1_1_reader_m_v_c.html#afe027439b26c41b3bb9be686ba3faea3',1,'MVCWebApp.Models.ReaderMVC.BirthPlace()']]],
  ['birthyear_26',['BirthYear',['../class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#afeaeabba5fa9a5c8b0f3c65a68cd8676',1,'BookShelf.Data.Entities.Authors.BirthYear()'],['../class_book_shelf_1_1_wpf_app_1_1_data_1_1_authors_w.html#a818c4f69881dc286c228ce12577566fa',1,'BookShelf.WpfApp.Data.AuthorsW.BirthYear()']]],
  ['bks_27',['Bks',['../class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#aadd70232fe9026f5a475f8a674a6a518',1,'BookShelf::Data::Entities::Authors']]],
  ['bl_28',['BL',['../namespace_book_shelf_1_1_wpf_app_1_1_b_l.html',1,'BookShelf::WpfApp']]],
  ['bok_29',['Bok',['../class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#a75f2ef5fd919b6a03db82fe50b18e8b2',1,'BookShelf::Data::Entities::Lends']]],
  ['bookdatas_30',['BookDatas',['../class_book_shelf_1_1_logic_1_1_book_datas.html',1,'BookShelf::Logic']]],
  ['bookid_31',['BookId',['../class_book_shelf_1_1_data_1_1_entities_1_1_books.html#ac57b1bd42cca27798e4375ba5a8131eb',1,'BookShelf.Data.Entities.Books.BookId()'],['../class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#a57b313af27ce0994eb2f423cdb0b2424',1,'BookShelf.Data.Entities.Lends.BookId()'],['../class_book_shelf_1_1_wpf_app_1_1_data_1_1_books_w.html#ae1fe3467a668520ac8a7e6ddc830e5dd',1,'BookShelf.WpfApp.Data.BooksW.BookId()'],['../class_book_shelf_1_1_wpf_app_1_1_data_1_1_lends_w.html#ab3da6a2374a5f12aef56f275e7666ee4',1,'BookShelf.WpfApp.Data.LendsW.BookId()']]],
  ['bookmanager_32',['BookManager',['../class_book_shelf_1_1_repository_1_1_book_manager.html',1,'BookShelf.Repository.BookManager'],['../class_book_shelf_1_1_repository_1_1_book_manager.html#ac8724acc8199c44e1a634d9a233d8b3f',1,'BookShelf.Repository.BookManager.BookManager()']]],
  ['books_33',['Books',['../class_book_shelf_1_1_data_1_1_entities_1_1_books.html',1,'BookShelf.Data.Entities.Books'],['../class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html#a1700ac9e5bd8449ae35f01fad66cbb97',1,'BookShelf.Data.Entities.BookShelfDBContext.Books()'],['../class_book_shelf_1_1_data_1_1_entities_1_1_books.html#a57daf86e932aebcb247c6507eff14b6e',1,'BookShelf.Data.Entities.Books.Books()']]],
  ['bookshelf_34',['BookShelf',['../namespace_book_shelf.html',1,'']]],
  ['bookshelfdbcontext_35',['BookShelfDBContext',['../class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html',1,'BookShelf.Data.Entities.BookShelfDBContext'],['../class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html#aedd8f9bbc447ffa75d3399f2ac3ff097',1,'BookShelf.Data.Entities.BookShelfDBContext.BookShelfDBContext()']]],
  ['booksw_36',['BooksW',['../class_book_shelf_1_1_wpf_app_1_1_data_1_1_books_w.html',1,'BookShelf::WpfApp::Data']]],
  ['data_37',['Data',['../namespace_book_shelf_1_1_data.html',1,'BookShelf.Data'],['../namespace_book_shelf_1_1_wpf_app_1_1_data.html',1,'BookShelf.WpfApp.Data']]],
  ['entities_38',['Entities',['../namespace_book_shelf_1_1_data_1_1_entities.html',1,'BookShelf::Data']]],
  ['logic_39',['Logic',['../namespace_book_shelf_1_1_logic.html',1,'BookShelf']]],
  ['repository_40',['Repository',['../namespace_book_shelf_1_1_repository.html',1,'BookShelf']]],
  ['tests_41',['Tests',['../namespace_book_shelf_1_1_logic_1_1_tests.html',1,'BookShelf::Logic']]],
  ['ui_42',['UI',['../namespace_book_shelf_1_1_wpf_app_1_1_u_i.html',1,'BookShelf::WpfApp']]],
  ['vm_43',['VM',['../namespace_book_shelf_1_1_wpf_app_1_1_v_m.html',1,'BookShelf::WpfApp']]],
  ['wpfapp_44',['WpfApp',['../namespace_book_shelf_1_1_wpf_app.html',1,'BookShelf']]]
];
