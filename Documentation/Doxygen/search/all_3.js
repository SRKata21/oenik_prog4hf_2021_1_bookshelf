var searchData=
[
  ['datas_84',['Datas',['../class_m_v_c_web_app_1_1_controllers_1_1_reader_controller.html#a9352f331fc69774a3493d3fabc2f00ee',1,'MVCWebApp::Controllers::ReaderController']]],
  ['delcmd_85',['DelCmd',['../class_wpf_client_1_1_main_v_m.html#a022cdab537a1a854a1d9f7eb668bfe2d',1,'WpfClient::MainVM']]],
  ['delete_86',['Delete',['../interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_logic.html#ab54a6a6561b63684380b7d56bae7f8e2',1,'BookShelf.WpfApp.BL.ILogic.Delete()'],['../class_book_shelf_1_1_wpf_app_1_1_b_l_1_1_lend_logic.html#ac34ac67e9034b1bae95b1bc62f5113b9',1,'BookShelf.WpfApp.BL.LendLogic.Delete()'],['../class_book_shelf_1_1_wpf_app_1_1_b_l_1_1_reader_logic.html#a86f39e79bcfd861caf19dccd857d7022',1,'BookShelf.WpfApp.BL.ReaderLogic.Delete()']]],
  ['deletecomm_87',['DeleteComm',['../class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_view_model.html#a144c08a799a1905543132be9106e3f48',1,'BookShelf::WpfApp::VM::ReaderViewModel']]],
  ['delonereader_88',['DelOneReader',['../class_m_v_c_web_app_1_1_controllers_1_1_reader_api_controller.html#aba80db80c8d66f753e61e6349dbb616d',1,'MVCWebApp::Controllers::ReaderApiController']]]
];
