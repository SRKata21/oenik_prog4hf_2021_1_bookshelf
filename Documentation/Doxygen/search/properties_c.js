var searchData=
[
  ['rdr_569',['Rdr',['../class_book_shelf_1_1_wpf_app_1_1_b_l_1_1_factory.html#aaabec71cd3ddb14a1ca5adc4307632e5',1,'BookShelf::WpfApp::BL::Factory']]],
  ['reader_570',['Reader',['../class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_administrator_window.html#a3203b8d1f95b06b753ba6ecd04546790',1,'BookShelf.WpfApp.UI.AdministratorWindow.Reader()'],['../class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_reader_data_editor_window.html#aeaa88bbd0a1d7abdb8f9f8dcdb83591b',1,'BookShelf.WpfApp.UI.ReaderDataEditorWindow.Reader()'],['../class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_reader_editor_window.html#aba0a3122f24250c4cba1675c2f12f4c1',1,'BookShelf.WpfApp.UI.ReaderEditorWindow.Reader()'],['../class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_editor_view_model.html#a3ebec52dcdefd34c455347f59d77c2ae',1,'BookShelf.WpfApp.VM.ReaderEditorViewModel.Reader()']]],
  ['readerlist_571',['ReaderList',['../class_m_v_c_web_app_1_1_models_1_1_reader_list_v_m.html#aceef1319ce090043438fe73cc44e5dd3',1,'MVCWebApp::Models::ReaderListVM']]],
  ['readername_572',['ReaderName',['../class_book_shelf_1_1_logic_1_1_lending_by_reader.html#a97fabbc84931fed13cb7483c2d42af36',1,'BookShelf::Logic::LendingByReader']]],
  ['requestid_573',['RequestId',['../class_m_v_c_web_app_1_1_models_1_1_error_view_model.html#a22fae77ef29e3c7df8f8e1364de3f28a',1,'MVCWebApp::Models::ErrorViewModel']]]
];
