var searchData=
[
  ['mainlogic_321',['MainLogic',['../class_wpf_client_1_1_main_logic.html',1,'WpfClient']]],
  ['mainvm_322',['MainVM',['../class_wpf_client_1_1_main_v_m.html',1,'WpfClient']]],
  ['mainwindow_323',['MainWindow',['../class_book_shelf_1_1_wpf_app_1_1_main_window.html',1,'BookShelf.WpfApp.MainWindow'],['../class_wpf_client_1_1_main_window.html',1,'WpfClient.MainWindow']]],
  ['manager_324',['Manager',['../class_book_shelf_1_1_repository_1_1_manager.html',1,'BookShelf::Repository']]],
  ['manager_3c_20authors_20_3e_325',['Manager&lt; Authors &gt;',['../class_book_shelf_1_1_repository_1_1_manager.html',1,'BookShelf::Repository']]],
  ['manager_3c_20books_20_3e_326',['Manager&lt; Books &gt;',['../class_book_shelf_1_1_repository_1_1_manager.html',1,'BookShelf::Repository']]],
  ['manager_3c_20lends_20_3e_327',['Manager&lt; Lends &gt;',['../class_book_shelf_1_1_repository_1_1_manager.html',1,'BookShelf::Repository']]],
  ['manager_3c_20people_20_3e_328',['Manager&lt; People &gt;',['../class_book_shelf_1_1_repository_1_1_manager.html',1,'BookShelf::Repository']]],
  ['mapperfactory_329',['MapperFactory',['../class_m_v_c_web_app_1_1_models_1_1_mapper_factory.html',1,'MVCWebApp::Models']]],
  ['myioc_330',['MyIoc',['../class_wpf_client_1_1_my_ioc.html',1,'WpfClient']]]
];
