var searchData=
[
  ['administratortest_271',['AdministratorTest',['../class_book_shelf_1_1_logic_1_1_tests_1_1_administrator_test.html',1,'BookShelf::Logic::Tests']]],
  ['administratoruser_272',['AdministratorUser',['../class_book_shelf_1_1_logic_1_1_administrator_user.html',1,'BookShelf::Logic']]],
  ['administratorwindow_273',['AdministratorWindow',['../class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_administrator_window.html',1,'BookShelf::WpfApp::UI']]],
  ['apiresult_274',['ApiResult',['../class_m_v_c_web_app_1_1_controllers_1_1_api_result.html',1,'MVCWebApp::Controllers']]],
  ['app_275',['App',['../class_wpf_client_1_1_app.html',1,'WpfClient.App'],['../class_book_shelf_1_1_wpf_app_1_1_app.html',1,'BookShelf.WpfApp.App']]],
  ['authormanager_276',['AuthorManager',['../class_book_shelf_1_1_repository_1_1_author_manager.html',1,'BookShelf::Repository']]],
  ['authors_277',['Authors',['../class_book_shelf_1_1_data_1_1_entities_1_1_authors.html',1,'BookShelf::Data::Entities']]],
  ['authorsw_278',['AuthorsW',['../class_book_shelf_1_1_wpf_app_1_1_data_1_1_authors_w.html',1,'BookShelf::WpfApp::Data']]]
];
