var searchData=
[
  ['iadministratoruser_291',['IAdministratorUser',['../interface_book_shelf_1_1_logic_1_1_i_administrator_user.html',1,'BookShelf::Logic']]],
  ['iauthormanager_292',['IAuthorManager',['../interface_book_shelf_1_1_repository_1_1_i_author_manager.html',1,'BookShelf::Repository']]],
  ['ibookmanager_293',['IBookManager',['../interface_book_shelf_1_1_repository_1_1_i_book_manager.html',1,'BookShelf::Repository']]],
  ['ieditorservice_294',['IEditorService',['../interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_editor_service.html',1,'BookShelf::WpfApp::BL']]],
  ['ilendmanager_295',['ILendManager',['../interface_book_shelf_1_1_repository_1_1_i_lend_manager.html',1,'BookShelf::Repository']]],
  ['ilibrarianuser_296',['ILibrarianUser',['../interface_book_shelf_1_1_logic_1_1_i_librarian_user.html',1,'BookShelf::Logic']]],
  ['ilogic_297',['ILogic',['../interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_logic.html',1,'BookShelf.WpfApp.BL.ILogic&lt; T &gt;'],['../interface_wpf_client_1_1_i_logic.html',1,'WpfClient.ILogic']]],
  ['ilogic_3c_20bookshelf_3a_3awpfapp_3a_3adata_3a_3alendsw_20_3e_298',['ILogic&lt; BookShelf::WpfApp::Data::LendsW &gt;',['../interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_logic.html',1,'BookShelf::WpfApp::BL']]],
  ['ilogic_3c_20bookshelf_3a_3awpfapp_3a_3adata_3a_3areadersw_20_3e_299',['ILogic&lt; BookShelf::WpfApp::Data::ReadersW &gt;',['../interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_logic.html',1,'BookShelf::WpfApp::BL']]],
  ['ilogic_3c_20lendsw_20_3e_300',['ILogic&lt; LendsW &gt;',['../interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_logic.html',1,'BookShelf::WpfApp::BL']]],
  ['ilogic_3c_20readersw_20_3e_301',['ILogic&lt; ReadersW &gt;',['../interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_logic.html',1,'BookShelf::WpfApp::BL']]],
  ['imanager_302',['IManager',['../interface_book_shelf_1_1_repository_1_1_i_manager.html',1,'BookShelf::Repository']]],
  ['imanager_3c_20authors_20_3e_303',['IManager&lt; Authors &gt;',['../interface_book_shelf_1_1_repository_1_1_i_manager.html',1,'BookShelf::Repository']]],
  ['imanager_3c_20books_20_3e_304',['IManager&lt; Books &gt;',['../interface_book_shelf_1_1_repository_1_1_i_manager.html',1,'BookShelf::Repository']]],
  ['imanager_3c_20lends_20_3e_305',['IManager&lt; Lends &gt;',['../interface_book_shelf_1_1_repository_1_1_i_manager.html',1,'BookShelf::Repository']]],
  ['imanager_3c_20people_20_3e_306',['IManager&lt; People &gt;',['../interface_book_shelf_1_1_repository_1_1_i_manager.html',1,'BookShelf::Repository']]],
  ['ipeoplemanager_307',['IPeopleManager',['../interface_book_shelf_1_1_repository_1_1_i_people_manager.html',1,'BookShelf::Repository']]],
  ['ireaderuser_308',['IReaderUser',['../interface_book_shelf_1_1_logic_1_1_i_reader_user.html',1,'BookShelf::Logic']]]
];
