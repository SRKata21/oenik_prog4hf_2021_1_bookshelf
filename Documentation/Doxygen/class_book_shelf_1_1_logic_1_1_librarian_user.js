var class_book_shelf_1_1_logic_1_1_librarian_user =
[
    [ "LibrarianUser", "class_book_shelf_1_1_logic_1_1_librarian_user.html#a16f95b62b7d9f145627c375d1f25cd17", null ],
    [ "ChangeAuthorName", "class_book_shelf_1_1_logic_1_1_librarian_user.html#ad7ece894a970d726b2a7e6c195206eb6", null ],
    [ "ChangeBookYear", "class_book_shelf_1_1_logic_1_1_librarian_user.html#ae33ea8f4b62d4677c9cc2f70ff477f25", null ],
    [ "ChangeLendGiveBackDate", "class_book_shelf_1_1_logic_1_1_librarian_user.html#a56971efb5f4d47664213f6fd1ad981b5", null ],
    [ "ChangePersonName", "class_book_shelf_1_1_logic_1_1_librarian_user.html#a06e69eb5dcac20d317d424846fde0eda", null ],
    [ "GetAllLends", "class_book_shelf_1_1_logic_1_1_librarian_user.html#a8cf52d8041e472c551024a2db3e06956", null ],
    [ "GetLendByReader", "class_book_shelf_1_1_logic_1_1_librarian_user.html#a06edbd4da4992d2a8d581634ebef61c1", null ],
    [ "GetOneAuthor", "class_book_shelf_1_1_logic_1_1_librarian_user.html#abfaaf5e0a5573b0df8f70684f3bab083", null ],
    [ "GetOneBook", "class_book_shelf_1_1_logic_1_1_librarian_user.html#a564be100ec2316a35f422211c909b137", null ],
    [ "GetOneLend", "class_book_shelf_1_1_logic_1_1_librarian_user.html#adb8ee552d86ad1b39eef0b09a9d37436", null ],
    [ "GetOnePerson", "class_book_shelf_1_1_logic_1_1_librarian_user.html#ad2e56300fae3869ecaf78337c32d4de2", null ],
    [ "InsertLend", "class_book_shelf_1_1_logic_1_1_librarian_user.html#ab6d61d08a6ab9af09a4ab3b87a46ddf4", null ],
    [ "TaskGetLendByReader", "class_book_shelf_1_1_logic_1_1_librarian_user.html#a231cc5bcee0fbded28ec1e912708609a", null ]
];