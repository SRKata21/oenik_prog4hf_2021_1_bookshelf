var namespace_book_shelf_1_1_logic =
[
    [ "Tests", "namespace_book_shelf_1_1_logic_1_1_tests.html", "namespace_book_shelf_1_1_logic_1_1_tests" ],
    [ "AdministratorUser", "class_book_shelf_1_1_logic_1_1_administrator_user.html", "class_book_shelf_1_1_logic_1_1_administrator_user" ],
    [ "BookDatas", "class_book_shelf_1_1_logic_1_1_book_datas.html", "class_book_shelf_1_1_logic_1_1_book_datas" ],
    [ "IAdministratorUser", "interface_book_shelf_1_1_logic_1_1_i_administrator_user.html", "interface_book_shelf_1_1_logic_1_1_i_administrator_user" ],
    [ "ILibrarianUser", "interface_book_shelf_1_1_logic_1_1_i_librarian_user.html", "interface_book_shelf_1_1_logic_1_1_i_librarian_user" ],
    [ "IReaderUser", "interface_book_shelf_1_1_logic_1_1_i_reader_user.html", "interface_book_shelf_1_1_logic_1_1_i_reader_user" ],
    [ "LendingByReader", "class_book_shelf_1_1_logic_1_1_lending_by_reader.html", "class_book_shelf_1_1_logic_1_1_lending_by_reader" ],
    [ "LibrarianUser", "class_book_shelf_1_1_logic_1_1_librarian_user.html", "class_book_shelf_1_1_logic_1_1_librarian_user" ],
    [ "ReaderUser", "class_book_shelf_1_1_logic_1_1_reader_user.html", "class_book_shelf_1_1_logic_1_1_reader_user" ]
];