var class_book_shelf_1_1_repository_1_1_author_manager =
[
    [ "AuthorManager", "class_book_shelf_1_1_repository_1_1_author_manager.html#ab24ab09db9ee25aee907a995d85d5c7a", null ],
    [ "ChangeBirthName", "class_book_shelf_1_1_repository_1_1_author_manager.html#a3c49f495d7627ce4a0ab40158e6ad277", null ],
    [ "ChangeCountry", "class_book_shelf_1_1_repository_1_1_author_manager.html#ae4115cf4b8a2d82f7efaabf882277f58", null ],
    [ "ChangeName", "class_book_shelf_1_1_repository_1_1_author_manager.html#ab47a3b9779d1f7c0856f8c138d9e6c60", null ],
    [ "ChangeNationality", "class_book_shelf_1_1_repository_1_1_author_manager.html#a85a1835c40c13da53fddf107ab6fe931", null ],
    [ "ChangeYear", "class_book_shelf_1_1_repository_1_1_author_manager.html#ad12d3415ae2b30528fd8df350247e18e", null ],
    [ "GetOne", "class_book_shelf_1_1_repository_1_1_author_manager.html#a67cb2758ebcaaadb83998e6169616fd3", null ]
];