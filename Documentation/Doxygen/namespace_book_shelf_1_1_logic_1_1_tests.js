var namespace_book_shelf_1_1_logic_1_1_tests =
[
    [ "AdministratorTest", "class_book_shelf_1_1_logic_1_1_tests_1_1_administrator_test.html", "class_book_shelf_1_1_logic_1_1_tests_1_1_administrator_test" ],
    [ "LibrarianTest", "class_book_shelf_1_1_logic_1_1_tests_1_1_librarian_test.html", "class_book_shelf_1_1_logic_1_1_tests_1_1_librarian_test" ],
    [ "LogicCreator", "class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html", "class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator" ],
    [ "ReaderTest", "class_book_shelf_1_1_logic_1_1_tests_1_1_reader_test.html", "class_book_shelf_1_1_logic_1_1_tests_1_1_reader_test" ]
];