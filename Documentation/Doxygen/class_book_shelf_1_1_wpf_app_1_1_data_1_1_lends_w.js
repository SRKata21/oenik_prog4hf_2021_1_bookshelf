var class_book_shelf_1_1_wpf_app_1_1_data_1_1_lends_w =
[
    [ "Convert", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_lends_w.html#a8456785d067a93e3a6b07ab83ecd9668", null ],
    [ "Copy", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_lends_w.html#a2a4e23f394d9a77998e59d91728d2508", null ],
    [ "ToString", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_lends_w.html#a78ea48cb1e842a162749ed5be0ee3a5a", null ],
    [ "BookId", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_lends_w.html#ab3da6a2374a5f12aef56f275e7666ee4", null ],
    [ "GivebackDate", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_lends_w.html#aeea5596d4b033135c5acaa9ce1d8ca54", null ],
    [ "LendDate", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_lends_w.html#a07332e5cfab6a90959ac959069760ff7", null ],
    [ "LendId", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_lends_w.html#a8c8ba1fd5d5e0f8a21d6a3fb13705e52", null ],
    [ "PersonId", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_lends_w.html#a6e47aaa2ab17b93d445e485220efc734", null ]
];