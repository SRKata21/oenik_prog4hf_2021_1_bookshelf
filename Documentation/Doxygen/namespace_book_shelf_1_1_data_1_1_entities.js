var namespace_book_shelf_1_1_data_1_1_entities =
[
    [ "Authors", "class_book_shelf_1_1_data_1_1_entities_1_1_authors.html", "class_book_shelf_1_1_data_1_1_entities_1_1_authors" ],
    [ "Books", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html", "class_book_shelf_1_1_data_1_1_entities_1_1_books" ],
    [ "BookShelfDBContext", "class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html", "class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context" ],
    [ "Lends", "class_book_shelf_1_1_data_1_1_entities_1_1_lends.html", "class_book_shelf_1_1_data_1_1_entities_1_1_lends" ],
    [ "People", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html", "class_book_shelf_1_1_data_1_1_entities_1_1_people" ]
];