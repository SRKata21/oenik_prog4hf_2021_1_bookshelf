var class_m_v_c_web_app_1_1_controllers_1_1_reader_api_controller =
[
    [ "ReaderApiController", "class_m_v_c_web_app_1_1_controllers_1_1_reader_api_controller.html#aa42e9eb117d153292b56c44da30b6b04", null ],
    [ "AddNewReader", "class_m_v_c_web_app_1_1_controllers_1_1_reader_api_controller.html#a52a5bb444dade0c9bae0b304c90ba2d0", null ],
    [ "DelOneReader", "class_m_v_c_web_app_1_1_controllers_1_1_reader_api_controller.html#aba80db80c8d66f753e61e6349dbb616d", null ],
    [ "GetAll", "class_m_v_c_web_app_1_1_controllers_1_1_reader_api_controller.html#aeddba44462cfb7da2e8ec38eb86e2af0", null ],
    [ "ModReader", "class_m_v_c_web_app_1_1_controllers_1_1_reader_api_controller.html#aa8062796b9d72b20d6c880c67186ef3f", null ]
];