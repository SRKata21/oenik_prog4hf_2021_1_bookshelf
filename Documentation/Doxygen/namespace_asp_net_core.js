var namespace_asp_net_core =
[
    [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
    [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
    [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
    [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
    [ "Views_Reader_ReaderDatas", "class_asp_net_core_1_1_views___reader___reader_datas.html", "class_asp_net_core_1_1_views___reader___reader_datas" ],
    [ "Views_Reader_ReaderEdit", "class_asp_net_core_1_1_views___reader___reader_edit.html", "class_asp_net_core_1_1_views___reader___reader_edit" ],
    [ "Views_Reader_ReaderIndex", "class_asp_net_core_1_1_views___reader___reader_index.html", "class_asp_net_core_1_1_views___reader___reader_index" ],
    [ "Views_Reader_ReaderList", "class_asp_net_core_1_1_views___reader___reader_list.html", "class_asp_net_core_1_1_views___reader___reader_list" ],
    [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
    [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
    [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ]
];