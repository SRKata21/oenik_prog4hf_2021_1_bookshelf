var class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_view_model =
[
    [ "ReaderViewModel", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_view_model.html#af64256d36de61239491afddc4021f31f", null ],
    [ "ReaderViewModel", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_view_model.html#a1b31d96a96827ce960eb65c607d0ccae", null ],
    [ "FillList", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_view_model.html#afc54a6d6f73b778cb596e205487e2c89", null ],
    [ "AllReader", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_view_model.html#a3de3e6f5576ebcaa2d34c2f235548109", null ],
    [ "DeleteComm", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_view_model.html#a144c08a799a1905543132be9106e3f48", null ],
    [ "ModifyComm", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_view_model.html#af1f89ea9cb0d7d3ae6dd5b5a760d2227", null ],
    [ "NewComm", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_view_model.html#adffbd0a2bd15aebdbe5387b470428928", null ],
    [ "SelectedReader", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_view_model.html#a1f8d0e61835e629d3eb56867e4b813d2", null ]
];