var namespace_m_v_c_web_app_1_1_controllers =
[
    [ "ApiResult", "class_m_v_c_web_app_1_1_controllers_1_1_api_result.html", "class_m_v_c_web_app_1_1_controllers_1_1_api_result" ],
    [ "HomeController", "class_m_v_c_web_app_1_1_controllers_1_1_home_controller.html", "class_m_v_c_web_app_1_1_controllers_1_1_home_controller" ],
    [ "ReaderApiController", "class_m_v_c_web_app_1_1_controllers_1_1_reader_api_controller.html", "class_m_v_c_web_app_1_1_controllers_1_1_reader_api_controller" ],
    [ "ReaderController", "class_m_v_c_web_app_1_1_controllers_1_1_reader_controller.html", "class_m_v_c_web_app_1_1_controllers_1_1_reader_controller" ]
];