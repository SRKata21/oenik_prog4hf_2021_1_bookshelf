var class_book_shelf_1_1_repository_1_1_manager =
[
    [ "Manager", "class_book_shelf_1_1_repository_1_1_manager.html#a1ae5997ebe852fd680c6df5a0196698b", null ],
    [ "GetAll", "class_book_shelf_1_1_repository_1_1_manager.html#a3354d58a1c1b404e0db2986b47ccefdc", null ],
    [ "GetOne", "class_book_shelf_1_1_repository_1_1_manager.html#ae3b94a9a495b5662f6582a1db606a0a4", null ],
    [ "Insert", "class_book_shelf_1_1_repository_1_1_manager.html#a6038a9b5d3a68d2cbc911da67d92d24d", null ],
    [ "Remove", "class_book_shelf_1_1_repository_1_1_manager.html#a0d03da54d66f2e80120e306ee8821357", null ],
    [ "Remove", "class_book_shelf_1_1_repository_1_1_manager.html#a9bb97671c859017028139b60a1b5c393", null ]
];