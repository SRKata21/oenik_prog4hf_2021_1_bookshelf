var namespace_book_shelf_1_1_wpf_app_1_1_u_i =
[
    [ "AdministratorWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_administrator_window.html", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_administrator_window" ],
    [ "EditorServiceViaWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_editor_service_via_window.html", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_editor_service_via_window" ],
    [ "LendEditorWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_lend_editor_window.html", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_lend_editor_window" ],
    [ "LibrarianWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_librarian_window.html", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_librarian_window" ],
    [ "ReaderDataEditorWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_reader_data_editor_window.html", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_reader_data_editor_window" ],
    [ "ReaderEditorWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_reader_editor_window.html", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_reader_editor_window" ],
    [ "ReaderWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_reader_window.html", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_reader_window" ]
];