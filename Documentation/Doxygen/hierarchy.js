var hierarchy =
[
    [ "BookShelf.Logic.Tests.AdministratorTest", "class_book_shelf_1_1_logic_1_1_tests_1_1_administrator_test.html", null ],
    [ "MVCWebApp.Controllers.ApiResult", "class_m_v_c_web_app_1_1_controllers_1_1_api_result.html", null ],
    [ "Application", null, [
      [ "BookShelf.WpfApp.App", "class_book_shelf_1_1_wpf_app_1_1_app.html", null ],
      [ "WpfClient.App", "class_wpf_client_1_1_app.html", null ]
    ] ],
    [ "BookShelf.Data.Entities.Authors", "class_book_shelf_1_1_data_1_1_entities_1_1_authors.html", null ],
    [ "BookShelf.Logic.BookDatas", "class_book_shelf_1_1_logic_1_1_book_datas.html", null ],
    [ "BookShelf.Data.Entities.Books", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html", null ],
    [ "Controller", null, [
      [ "MVCWebApp.Controllers.HomeController", "class_m_v_c_web_app_1_1_controllers_1_1_home_controller.html", null ],
      [ "MVCWebApp.Controllers.ReaderApiController", "class_m_v_c_web_app_1_1_controllers_1_1_reader_api_controller.html", null ],
      [ "MVCWebApp.Controllers.ReaderController", "class_m_v_c_web_app_1_1_controllers_1_1_reader_controller.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "BookShelf.Data.Entities.BookShelfDBContext", "class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html", null ]
    ] ],
    [ "MVCWebApp.Models.ErrorViewModel", "class_m_v_c_web_app_1_1_models_1_1_error_view_model.html", null ],
    [ "BookShelf.WpfApp.BL.Factory", "class_book_shelf_1_1_wpf_app_1_1_b_l_1_1_factory.html", null ],
    [ "BookShelf.Logic.IAdministratorUser", "interface_book_shelf_1_1_logic_1_1_i_administrator_user.html", [
      [ "BookShelf.Logic.AdministratorUser", "class_book_shelf_1_1_logic_1_1_administrator_user.html", null ]
    ] ],
    [ "IComponentConnector", null, [
      [ "BookShelf.WpfApp.UI.AdministratorWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_administrator_window.html", null ],
      [ "BookShelf.WpfApp.UI.LendEditorWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_lend_editor_window.html", null ],
      [ "BookShelf.WpfApp.UI.LibrarianWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_librarian_window.html", null ],
      [ "BookShelf.WpfApp.UI.ReaderDataEditorWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_reader_data_editor_window.html", null ],
      [ "BookShelf.WpfApp.UI.ReaderEditorWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_reader_editor_window.html", null ],
      [ "BookShelf.WpfApp.UI.ReaderWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_reader_window.html", null ]
    ] ],
    [ "BookShelf.WpfApp.BL.IEditorService", "interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_editor_service.html", [
      [ "BookShelf.WpfApp.UI.EditorServiceViaWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "BookShelf.Logic.ILibrarianUser", "interface_book_shelf_1_1_logic_1_1_i_librarian_user.html", [
      [ "BookShelf.Logic.LibrarianUser", "class_book_shelf_1_1_logic_1_1_librarian_user.html", null ]
    ] ],
    [ "BookShelf.WpfApp.BL.ILogic< T >", "interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_logic.html", null ],
    [ "WpfClient.ILogic", "interface_wpf_client_1_1_i_logic.html", [
      [ "WpfClient.MainLogic", "class_wpf_client_1_1_main_logic.html", null ]
    ] ],
    [ "BookShelf.WpfApp.BL.ILogic< BookShelf.WpfApp.Data.LendsW >", "interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_logic.html", null ],
    [ "BookShelf.WpfApp.BL.ILogic< BookShelf.WpfApp.Data.ReadersW >", "interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_logic.html", null ],
    [ "BookShelf.WpfApp.BL.ILogic< LendsW >", "interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_logic.html", [
      [ "BookShelf.WpfApp.BL.LendLogic", "class_book_shelf_1_1_wpf_app_1_1_b_l_1_1_lend_logic.html", null ]
    ] ],
    [ "BookShelf.WpfApp.BL.ILogic< ReadersW >", "interface_book_shelf_1_1_wpf_app_1_1_b_l_1_1_i_logic.html", [
      [ "BookShelf.WpfApp.BL.ReaderLogic", "class_book_shelf_1_1_wpf_app_1_1_b_l_1_1_reader_logic.html", null ]
    ] ],
    [ "BookShelf.Repository.IManager< T >", "interface_book_shelf_1_1_repository_1_1_i_manager.html", [
      [ "BookShelf.Repository.Manager< T >", "class_book_shelf_1_1_repository_1_1_manager.html", null ]
    ] ],
    [ "BookShelf.Repository.IManager< Authors >", "interface_book_shelf_1_1_repository_1_1_i_manager.html", [
      [ "BookShelf.Repository.IAuthorManager", "interface_book_shelf_1_1_repository_1_1_i_author_manager.html", [
        [ "BookShelf.Repository.AuthorManager", "class_book_shelf_1_1_repository_1_1_author_manager.html", null ]
      ] ]
    ] ],
    [ "BookShelf.Repository.IManager< Books >", "interface_book_shelf_1_1_repository_1_1_i_manager.html", [
      [ "BookShelf.Repository.IBookManager", "interface_book_shelf_1_1_repository_1_1_i_book_manager.html", [
        [ "BookShelf.Repository.BookManager", "class_book_shelf_1_1_repository_1_1_book_manager.html", null ]
      ] ]
    ] ],
    [ "BookShelf.Repository.IManager< Lends >", "interface_book_shelf_1_1_repository_1_1_i_manager.html", [
      [ "BookShelf.Repository.ILendManager", "interface_book_shelf_1_1_repository_1_1_i_lend_manager.html", [
        [ "BookShelf.Repository.LendManager", "class_book_shelf_1_1_repository_1_1_lend_manager.html", null ]
      ] ]
    ] ],
    [ "BookShelf.Repository.IManager< People >", "interface_book_shelf_1_1_repository_1_1_i_manager.html", [
      [ "BookShelf.Repository.IPeopleManager", "interface_book_shelf_1_1_repository_1_1_i_people_manager.html", [
        [ "BookShelf.Repository.PeopleManager", "class_book_shelf_1_1_repository_1_1_people_manager.html", null ]
      ] ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "BookShelf.Logic.IReaderUser", "interface_book_shelf_1_1_logic_1_1_i_reader_user.html", [
      [ "BookShelf.Logic.ReaderUser", "class_book_shelf_1_1_logic_1_1_reader_user.html", null ]
    ] ],
    [ "IServiceLocator", null, [
      [ "WpfClient.MyIoc", "class_wpf_client_1_1_my_ioc.html", null ]
    ] ],
    [ "BookShelf.Logic.LendingByReader", "class_book_shelf_1_1_logic_1_1_lending_by_reader.html", null ],
    [ "BookShelf.Data.Entities.Lends", "class_book_shelf_1_1_data_1_1_entities_1_1_lends.html", null ],
    [ "BookShelf.Logic.Tests.LibrarianTest", "class_book_shelf_1_1_logic_1_1_tests_1_1_librarian_test.html", null ],
    [ "BookShelf.Logic.Tests.LogicCreator", "class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html", null ],
    [ "BookShelf.Repository.Manager< Authors >", "class_book_shelf_1_1_repository_1_1_manager.html", [
      [ "BookShelf.Repository.AuthorManager", "class_book_shelf_1_1_repository_1_1_author_manager.html", null ]
    ] ],
    [ "BookShelf.Repository.Manager< Books >", "class_book_shelf_1_1_repository_1_1_manager.html", [
      [ "BookShelf.Repository.BookManager", "class_book_shelf_1_1_repository_1_1_book_manager.html", null ]
    ] ],
    [ "BookShelf.Repository.Manager< Lends >", "class_book_shelf_1_1_repository_1_1_manager.html", [
      [ "BookShelf.Repository.LendManager", "class_book_shelf_1_1_repository_1_1_lend_manager.html", null ]
    ] ],
    [ "BookShelf.Repository.Manager< People >", "class_book_shelf_1_1_repository_1_1_manager.html", [
      [ "BookShelf.Repository.PeopleManager", "class_book_shelf_1_1_repository_1_1_people_manager.html", null ]
    ] ],
    [ "MVCWebApp.Models.MapperFactory", "class_m_v_c_web_app_1_1_models_1_1_mapper_factory.html", null ],
    [ "ObservableObject", null, [
      [ "BookShelf.WpfApp.Data.AuthorsW", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_authors_w.html", null ],
      [ "BookShelf.WpfApp.Data.BooksW", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_books_w.html", null ],
      [ "BookShelf.WpfApp.Data.LendsW", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_lends_w.html", null ],
      [ "BookShelf.WpfApp.Data.ReadersW", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w.html", null ],
      [ "WpfClient.ReaderVM", "class_wpf_client_1_1_reader_v_m.html", null ]
    ] ],
    [ "BookShelf.Data.Entities.People", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html", null ],
    [ "MVCWebApp.Program", "class_m_v_c_web_app_1_1_program.html", null ],
    [ "RazorPage", null, [
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Reader_ReaderDatas", "class_asp_net_core_1_1_views___reader___reader_datas.html", null ],
      [ "AspNetCore.Views_Reader_ReaderEdit", "class_asp_net_core_1_1_views___reader___reader_edit.html", null ],
      [ "AspNetCore.Views_Reader_ReaderIndex", "class_asp_net_core_1_1_views___reader___reader_index.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ]
    ] ],
    [ "RazorPage< IEnumerable< ReaderMVC >>", null, [
      [ "AspNetCore.Views_Reader_ReaderList", "class_asp_net_core_1_1_views___reader___reader_list.html", null ]
    ] ],
    [ "MVCWebApp.Models.ReaderListVM", "class_m_v_c_web_app_1_1_models_1_1_reader_list_v_m.html", null ],
    [ "MVCWebApp.Models.ReaderMVC", "class_m_v_c_web_app_1_1_models_1_1_reader_m_v_c.html", null ],
    [ "BookShelf.Logic.Tests.ReaderTest", "class_book_shelf_1_1_logic_1_1_tests_1_1_reader_test.html", null ],
    [ "SimpleIoc", null, [
      [ "WpfClient.MyIoc", "class_wpf_client_1_1_my_ioc.html", null ]
    ] ],
    [ "MVCWebApp.Startup", "class_m_v_c_web_app_1_1_startup.html", null ],
    [ "ViewModelBase", null, [
      [ "BookShelf.WpfApp.VM.LendEditorViewModel", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_lend_editor_view_model.html", null ],
      [ "BookShelf.WpfApp.VM.LibrarianViewModel", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_librarian_view_model.html", null ],
      [ "BookShelf.WpfApp.VM.ReaderEditorViewModel", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_editor_view_model.html", null ],
      [ "BookShelf.WpfApp.VM.ReaderViewModel", "class_book_shelf_1_1_wpf_app_1_1_v_m_1_1_reader_view_model.html", null ],
      [ "WpfClient.MainVM", "class_wpf_client_1_1_main_v_m.html", null ]
    ] ],
    [ "Window", null, [
      [ "BookShelf.WpfApp.MainWindow", "class_book_shelf_1_1_wpf_app_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "BookShelf.WpfApp.UI.AdministratorWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_administrator_window.html", null ],
      [ "BookShelf.WpfApp.UI.LendEditorWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_lend_editor_window.html", null ],
      [ "BookShelf.WpfApp.UI.LibrarianWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_librarian_window.html", null ],
      [ "BookShelf.WpfApp.UI.ReaderDataEditorWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_reader_data_editor_window.html", null ],
      [ "BookShelf.WpfApp.UI.ReaderEditorWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_reader_editor_window.html", null ],
      [ "BookShelf.WpfApp.UI.ReaderWindow", "class_book_shelf_1_1_wpf_app_1_1_u_i_1_1_reader_window.html", null ],
      [ "WpfClient.EditorWindow", "class_wpf_client_1_1_editor_window.html", null ],
      [ "WpfClient.EditorWindowMore", "class_wpf_client_1_1_editor_window_more.html", null ],
      [ "WpfClient.MainWindow", "class_wpf_client_1_1_main_window.html", null ]
    ] ]
];