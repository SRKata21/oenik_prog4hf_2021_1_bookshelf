var class_book_shelf_1_1_wpf_app_1_1_data_1_1_authors_w =
[
    [ "Convert", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_authors_w.html#a11188804050435e58deb7aa1f91bb4e7", null ],
    [ "Copy", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_authors_w.html#a0c5c0aeccc57bf23bc8cf847570f5188", null ],
    [ "AuthorId", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_authors_w.html#ac9caeec148d3187d8b15592e16fa2faf", null ],
    [ "BirthCountry", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_authors_w.html#a14738e608e383c75a15f1979610f1f5e", null ],
    [ "BirthName", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_authors_w.html#a790caacdb2ca15803eac2be0c1aa9d62", null ],
    [ "BirthYear", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_authors_w.html#a818c4f69881dc286c228ce12577566fa", null ],
    [ "Name", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_authors_w.html#a327cdcd50f4327cfbf514169b409d052", null ],
    [ "Nationality", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_authors_w.html#aa0b86ebe2b1f280cb653364f52c89b5c", null ]
];