var namespace_m_v_c_web_app_1_1_models =
[
    [ "ErrorViewModel", "class_m_v_c_web_app_1_1_models_1_1_error_view_model.html", "class_m_v_c_web_app_1_1_models_1_1_error_view_model" ],
    [ "MapperFactory", "class_m_v_c_web_app_1_1_models_1_1_mapper_factory.html", "class_m_v_c_web_app_1_1_models_1_1_mapper_factory" ],
    [ "ReaderListVM", "class_m_v_c_web_app_1_1_models_1_1_reader_list_v_m.html", "class_m_v_c_web_app_1_1_models_1_1_reader_list_v_m" ],
    [ "ReaderMVC", "class_m_v_c_web_app_1_1_models_1_1_reader_m_v_c.html", "class_m_v_c_web_app_1_1_models_1_1_reader_m_v_c" ]
];