var interface_book_shelf_1_1_logic_1_1_i_administrator_user =
[
    [ "GetAllPeople", "interface_book_shelf_1_1_logic_1_1_i_administrator_user.html#a15e577439f3d1ca2818ae5a9dd67a17c", null ],
    [ "GetOnePerson", "interface_book_shelf_1_1_logic_1_1_i_administrator_user.html#ab76446fcf351fa257d6e9cef19a8d396", null ],
    [ "InsertAuthor", "interface_book_shelf_1_1_logic_1_1_i_administrator_user.html#af37c01ec445c4a4bb2180266d8b5d7c3", null ],
    [ "InsertBook", "interface_book_shelf_1_1_logic_1_1_i_administrator_user.html#a2b5d7c07e543bf52b654a8b0db3ad074", null ],
    [ "InsertPerson", "interface_book_shelf_1_1_logic_1_1_i_administrator_user.html#a974170a2874c8028498a74783affd3ce", null ],
    [ "RemoveAuthorByKey", "interface_book_shelf_1_1_logic_1_1_i_administrator_user.html#a71659447a0586a2859459c43de661eb3", null ],
    [ "RemoveBookByKey", "interface_book_shelf_1_1_logic_1_1_i_administrator_user.html#aa4fe69115c3c4624f4fe3192c646ac19", null ],
    [ "RemovePersonByKey", "interface_book_shelf_1_1_logic_1_1_i_administrator_user.html#ad0a6ac50c6514058c5b983e796dab842", null ]
];