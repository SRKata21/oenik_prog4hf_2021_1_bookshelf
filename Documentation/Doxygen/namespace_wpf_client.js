var namespace_wpf_client =
[
    [ "App", "class_wpf_client_1_1_app.html", "class_wpf_client_1_1_app" ],
    [ "EditorWindow", "class_wpf_client_1_1_editor_window.html", "class_wpf_client_1_1_editor_window" ],
    [ "EditorWindowMore", "class_wpf_client_1_1_editor_window_more.html", "class_wpf_client_1_1_editor_window_more" ],
    [ "ILogic", "interface_wpf_client_1_1_i_logic.html", "interface_wpf_client_1_1_i_logic" ],
    [ "MainLogic", "class_wpf_client_1_1_main_logic.html", "class_wpf_client_1_1_main_logic" ],
    [ "MainVM", "class_wpf_client_1_1_main_v_m.html", "class_wpf_client_1_1_main_v_m" ],
    [ "MainWindow", "class_wpf_client_1_1_main_window.html", "class_wpf_client_1_1_main_window" ],
    [ "MyIoc", "class_wpf_client_1_1_my_ioc.html", "class_wpf_client_1_1_my_ioc" ],
    [ "ReaderVM", "class_wpf_client_1_1_reader_v_m.html", "class_wpf_client_1_1_reader_v_m" ]
];