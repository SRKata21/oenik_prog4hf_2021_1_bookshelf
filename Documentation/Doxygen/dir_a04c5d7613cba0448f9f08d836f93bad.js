var dir_a04c5d7613cba0448f9f08d836f93bad =
[
    [ "BookShelf.Data", "dir_b8e679d647bc251f156d3d0cda26df8b.html", "dir_b8e679d647bc251f156d3d0cda26df8b" ],
    [ "BookShelf.Logic", "dir_b2d220865ef9acca5075403cd030c7d4.html", "dir_b2d220865ef9acca5075403cd030c7d4" ],
    [ "BookShelf.Logic.Tests", "dir_c85c78abe2eb0a09d9359abd4829f0cd.html", "dir_c85c78abe2eb0a09d9359abd4829f0cd" ],
    [ "BookShelf.Repository", "dir_8507d05d0a96ed4aa16d55b729960342.html", "dir_8507d05d0a96ed4aa16d55b729960342" ],
    [ "BookShelf.WpfApp", "dir_27951eb0120a81d0389aa350f93f6b3f.html", "dir_27951eb0120a81d0389aa350f93f6b3f" ],
    [ "MVCWebApp", "dir_7a825bba4313249d5417a15f434c4806.html", "dir_7a825bba4313249d5417a15f434c4806" ],
    [ "WpfClient", "dir_83cc992c863fef8ba3fc1fcc96d71a86.html", "dir_83cc992c863fef8ba3fc1fcc96d71a86" ]
];