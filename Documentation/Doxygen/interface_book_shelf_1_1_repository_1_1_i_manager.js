var interface_book_shelf_1_1_repository_1_1_i_manager =
[
    [ "GetAll", "interface_book_shelf_1_1_repository_1_1_i_manager.html#a5759f70d1709b6b198b927f43f118214", null ],
    [ "GetOne", "interface_book_shelf_1_1_repository_1_1_i_manager.html#a3341cfc14cd3a47d3f978e7c552c44ab", null ],
    [ "Insert", "interface_book_shelf_1_1_repository_1_1_i_manager.html#ab1fc09fd8bf2a0e0d1f0424f3b4f5276", null ],
    [ "Remove", "interface_book_shelf_1_1_repository_1_1_i_manager.html#a3fe7a43d69f42e6e2c08785d4b3d5ed5", null ],
    [ "Remove", "interface_book_shelf_1_1_repository_1_1_i_manager.html#a692b61b884750ad4e575a4578e75dd23", null ]
];