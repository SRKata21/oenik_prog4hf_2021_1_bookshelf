var class_book_shelf_1_1_wpf_app_1_1_data_1_1_books_w =
[
    [ "Convert", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_books_w.html#aef91e08fd7639e018029eccdaffdca7a", null ],
    [ "Copy", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_books_w.html#ae00becea3cc18f97c7b1418cbbaaf163", null ],
    [ "AuthorId", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_books_w.html#ae512594cf9ccd6563fb884c60196217f", null ],
    [ "BookId", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_books_w.html#ae1fe3467a668520ac8a7e6ddc830e5dd", null ],
    [ "GetDate", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_books_w.html#aa08cc0484e29a8c973e784b783896470", null ],
    [ "Pages", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_books_w.html#ab1cd129d2d41e49c019f778cd96baf50", null ],
    [ "Place", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_books_w.html#a2214420d896a5d05cbe999f9148cee0f", null ],
    [ "Publisher", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_books_w.html#af4cd5d22ae899d072e8a684c0d7bde31", null ],
    [ "Title", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_books_w.html#a7c87f4befdd0193f40ba6980610540c2", null ],
    [ "Year", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_books_w.html#a015f52fc0bbea55829c1f55c9b8a9334", null ]
];