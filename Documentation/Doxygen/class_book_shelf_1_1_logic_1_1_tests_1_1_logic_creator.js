var class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator =
[
    [ "CreateLibrarianLogicWithMocks", "class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#ae77c8209d432a57543c67f86bca3e910", null ],
    [ "CreateReaderLogicWithMocks", "class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#a766aefb41a2673d3143ce8181658f4c7", null ],
    [ "ExpectedFamBooks", "class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#a770b6af2450214e9b935300d8fcc6434", null ],
    [ "ExpectedHunBooks", "class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#ad94e6e061063da93a6310d3888e316e1", null ],
    [ "ExpectedLends", "class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#a89ce9757c74aeaf22ae79f2690c2c5c5", null ],
    [ "MockAuthorRepo", "class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#ad6b5c622cb139e42e601633bef77b97b", null ],
    [ "MockBookRepo", "class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#a47ff680302e2f70dc5b63909bab5a874", null ],
    [ "MockLendRepo", "class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#aecba52869f40e99b55bd06531b16a9a9", null ],
    [ "MockPeopleRepo", "class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#a32b858e47a7bbf4a7b7b19b47c3ddf15", null ]
];