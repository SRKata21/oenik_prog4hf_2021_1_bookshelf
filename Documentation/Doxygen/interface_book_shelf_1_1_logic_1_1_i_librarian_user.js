var interface_book_shelf_1_1_logic_1_1_i_librarian_user =
[
    [ "ChangeAuthorName", "interface_book_shelf_1_1_logic_1_1_i_librarian_user.html#ad206151142b90aac35e9d07d0b53cfa4", null ],
    [ "ChangeBookYear", "interface_book_shelf_1_1_logic_1_1_i_librarian_user.html#a14d669e633c7bfc9e60633ff0621e56d", null ],
    [ "ChangeLendGiveBackDate", "interface_book_shelf_1_1_logic_1_1_i_librarian_user.html#a01f531b9d8f5d2d2e849c09c99d880e3", null ],
    [ "GetAllLends", "interface_book_shelf_1_1_logic_1_1_i_librarian_user.html#a0eaebae228ed0b4cacf8ca24b92d685a", null ],
    [ "GetLendByReader", "interface_book_shelf_1_1_logic_1_1_i_librarian_user.html#a7f1c548f09b6dae1e4dd608ed24bd856", null ],
    [ "GetOneAuthor", "interface_book_shelf_1_1_logic_1_1_i_librarian_user.html#a504b8cae44e5b391f95a4ae8b12c444d", null ],
    [ "GetOneBook", "interface_book_shelf_1_1_logic_1_1_i_librarian_user.html#a6ba111cff734a3f7dfbe3bfd54146d3f", null ],
    [ "GetOneLend", "interface_book_shelf_1_1_logic_1_1_i_librarian_user.html#a245a9a8dd6c3f605103d5d7e3413edbf", null ],
    [ "GetOnePerson", "interface_book_shelf_1_1_logic_1_1_i_librarian_user.html#a3159514be6d30831c977e54ec224d5fc", null ],
    [ "InsertLend", "interface_book_shelf_1_1_logic_1_1_i_librarian_user.html#a42c2dcaefb0ecc92759d32cb962ac2e7", null ]
];