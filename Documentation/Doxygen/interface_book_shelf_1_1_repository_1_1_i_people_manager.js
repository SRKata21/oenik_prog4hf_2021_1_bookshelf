var interface_book_shelf_1_1_repository_1_1_i_people_manager =
[
    [ "ChangeAddress", "interface_book_shelf_1_1_repository_1_1_i_people_manager.html#a539462d1eec019bcb351339a22ef4ddc", null ],
    [ "ChangeBirthDate", "interface_book_shelf_1_1_repository_1_1_i_people_manager.html#a3fc80d5bdd96b615cd672bc6b857786f", null ],
    [ "ChangeBirthPlace", "interface_book_shelf_1_1_repository_1_1_i_people_manager.html#ad08573558c3bbdec728e64fdf277ccc5", null ],
    [ "ChangeEmail", "interface_book_shelf_1_1_repository_1_1_i_people_manager.html#ab79a66440e1061812e6a2ecccb05bf52", null ],
    [ "ChangeEnterDate", "interface_book_shelf_1_1_repository_1_1_i_people_manager.html#aaaf372cc5bff49444d33c9d5e70652ce", null ],
    [ "ChangeMotherName", "interface_book_shelf_1_1_repository_1_1_i_people_manager.html#a505e3f4d208df4d8a096abf0ca73d788", null ],
    [ "ChangeName", "interface_book_shelf_1_1_repository_1_1_i_people_manager.html#ab3fc78387bde9e82a90d09763789e615", null ]
];