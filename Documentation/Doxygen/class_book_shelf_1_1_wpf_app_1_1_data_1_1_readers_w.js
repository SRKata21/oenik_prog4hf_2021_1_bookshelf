var class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w =
[
    [ "Convert", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w.html#acf9dfaedf8b84144eeb9220fa5daa9f8", null ],
    [ "Copy", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w.html#a4295bf071fc20f630101dfc2a017882e", null ],
    [ "ToString", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w.html#a0f87402c97eb510f039dbf51de493613", null ],
    [ "Address", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w.html#a68a601c8103e1e9109a1881fa73dee41", null ],
    [ "BirthDate", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w.html#a5649a29ebb16616cad21c55fe5a47d59", null ],
    [ "BirthPlace", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w.html#afa0b34c80f943e46d4d063690a778b4b", null ],
    [ "Email", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w.html#aca98c374f4f306a6e489fc53c605a0a2", null ],
    [ "EnterDate", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w.html#a122d9ec1b4feabc1448895ae10a8f666", null ],
    [ "MotherName", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w.html#aaf333e25ae2b9cfac5c389cbde0e95fc", null ],
    [ "Name", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w.html#ac9cae2f6e525f867bfddd2ec815159d5", null ],
    [ "PersonId", "class_book_shelf_1_1_wpf_app_1_1_data_1_1_readers_w.html#a1cd17e9275d6a01c2e9da3594e26ccc1", null ]
];