﻿namespace MVCWebApp.Controllers
{
    /// <summary>
    /// Helper class to return the result of a methode.
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether the operation was succeded or not.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}