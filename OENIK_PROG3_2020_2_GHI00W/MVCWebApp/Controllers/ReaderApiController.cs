﻿namespace MVCWebApp.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using BookShelf.Data.Entities;
    using BookShelf.Logic;
    using Microsoft.AspNetCore.Mvc;
    using MVCWebApp.Models;

    /// <summary>
    /// Controller for reader table for API.
    /// </summary>
    public class ReaderApiController : Controller
    {
        private IAdministratorUser adminLogic;
        private IReaderUser readerLogic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderApiController"/> class.
        /// </summary>
        /// <param name="aLogic">The Administrator Logic instance.</param>
        /// <param name="rLogic">The reader Logic instance.</param>
        /// <param name="mapper">The mapper instance.</param>
        public ReaderApiController(IAdministratorUser aLogic, IReaderUser rLogic, IMapper mapper)
        {
            adminLogic = aLogic;
            readerLogic = rLogic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Gets all reader by logic.
        /// </summary>
        /// <returns>The list of the readers.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<ReaderMVC> GetAll()
        {
            var people = adminLogic.GetAllPeople();
            return mapper.Map<IList<People>, List<ReaderMVC>>(people);
        }

        /// <summary>
        /// Delete the reader by logic.
        /// </summary>
        /// <param name="id">The id of the reader that should be deleted.</param>
        /// <returns>The operation was succeded or not.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOneReader(int id)
        {
            return new ApiResult() { OperationResult = adminLogic.RemovePersonByKey(id) };
        }

        /// <summary>
        /// Add new reader by logic.
        /// </summary>
        /// <param name="reader">The reader that should be added.</param>
        /// <returns>The operation was succeded or not.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddNewReader(ReaderMVC reader)
        {
            bool success = true;
            try
            {
                if (reader != null)
                {
                    reader.MotherName = "Default";
                    reader.BirthPlace = "Default";
                    People people = mapper.Map<ReaderMVC, People>(reader);
                    adminLogic.InsertPerson(people);
                }
            }
            catch (ArgumentException ex)
            {
                success = (!string.IsNullOrEmpty(ex.Message)) ? false : false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// Modify the reader by logic.
        /// </summary>
        /// <param name="reader">The reader that should be modified.</param>
        /// <returns>The operation was succeded or not.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModReader(ReaderMVC reader)
        {
            if (reader != null)
            {
                ApiResult res = new ApiResult() { OperationResult = readerLogic.ChangePersonAddress(reader.PersonId, reader.Address) };
                if (res.OperationResult)
                {
                    res = new ApiResult() { OperationResult = readerLogic.ChangePersonEmail(reader.PersonId, reader.Email) };
                }

                return res;
            }

            return new ApiResult() { OperationResult = false };
        }
    }
}
