﻿namespace MVCWebApp.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using BookShelf.Data.Entities;
    using BookShelf.Logic;
    using Microsoft.AspNetCore.Mvc;
    using MVCWebApp.Models;

    /// <summary>
    /// The reader controller.
    /// </summary>
    public class ReaderController : Controller
    {
        private IAdministratorUser adminLogic;
        private IReaderUser readerLogic;
        private IMapper mapper;
        private ReaderListVM vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderController"/> class.
        /// </summary>
        /// <param name="aLogic">The Administrator Logic instance.</param>
        /// <param name="rLogic">The reader Logic instance.</param>
        /// <param name="mapper">The mapper instance.</param>
        public ReaderController(IAdministratorUser aLogic, IReaderUser rLogic, IMapper mapper)
        {
            adminLogic = aLogic;
            readerLogic = rLogic;
            this.mapper = mapper;
            ViewData["editAction"] = string.Empty;

            vm = new ReaderListVM();
            vm.EditedReader = new ReaderMVC();

            var readers = adminLogic.GetAllPeople();

            var list = mapper?.Map<IList<People>, List<ReaderMVC>>(readers);
            Collection<ReaderMVC> rlist = new Collection<ReaderMVC>();
            foreach (var item in list)
            {
                rlist.Add(item);
            }

            vm.SetReaderList(rlist);
        }

        /// <summary>
        /// Displays the start page.
        /// </summary>
        /// <returns>The view of start page.</returns>
        public IActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("ReaderIndex", vm);
        }

        // GET: Readers/Datas/5

        /// <summary>
        /// Displays the one loaded reader from database.
        /// </summary>
        /// <param name="id">The id of the reader.</param>
        /// <returns>The view.</returns>
        public IActionResult Datas(int id)
        {
            return View("ReaderDatas", GetReaderModel(id));
        }

        // GET: Readers/Datas/5

        /// <summary>
        /// Tries Removing the reader.
        /// </summary>
        /// <param name="id">The id of the reader.</param>
        /// <returns>The view.</returns>
        public IActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete Fail";
            if (adminLogic.RemovePersonByKey(id))
            {
                TempData["editResult"] = "Delete Ok";
            }

            return RedirectToAction(nameof(Index));
        }

        // GET: Readers/Edit/5

        /// <summary>
        /// Tries loading the reader and display to index view editor.
        /// </summary>
        /// <param name="id">The id of the reader.</param>
        /// <returns>The view.</returns>
        public IActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedReader = GetReaderModel(id);
            return View("ReaderIndex", vm);
        }

        // POST: Readers/Edit/5

        /// <summary>
        /// Tries editing the reader and redirect to the Index.
        /// </summary>
        /// <param name="reader">The edited reader.</param>
        /// <param name="editAction">The editActon.</param>
        /// <returns>The displaying view.</returns>
        [HttpPost]
        public IActionResult Edit(ReaderMVC reader, string editAction)
        {
            if (ModelState.IsValid && reader != null)
            {
                TempData["editResult"] = "Edit Ok";
                if (editAction == "AddNew")
                {
                    try
                    {
                        reader.MotherName = "Default";
                        reader.BirthPlace = "Default";
                        People newP = mapper.Map<ReaderMVC, People>(reader);
                        adminLogic.InsertPerson(newP);
                    }
                    catch (ArgumentException ex)
                    {
                        TempData["editResult"] = "Insert reader Fail" + ex.Message;
                    }
                }
                else
                {
                    if ((!readerLogic.ChangePersonEmail(reader.PersonId, reader.Email)) || (!readerLogic.ChangePersonAddress(reader.PersonId, reader.Address)))
                    {
                        TempData["editResult"] = "Edit Fail";
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedReader = reader;
                return View("ReaderIndex", vm);
            }
        }

        private ReaderMVC GetReaderModel(int id)
        {
            People oneReader = adminLogic.GetOnePerson(id);
            return mapper.Map<People, ReaderMVC>(oneReader);
        }
    }
}
