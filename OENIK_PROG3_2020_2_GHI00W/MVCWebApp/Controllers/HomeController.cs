﻿namespace MVCWebApp.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using MVCWebApp.Models;

    /// <summary>
    /// The home controller.
    /// </summary>
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="logger">The object to log.</param>
        public HomeController(ILogger<HomeController> logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// The motor of the actions.
        /// </summary>
        /// <returns>The View that is used.</returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Privacy methode in controller.
        /// </summary>
        /// <returns>The View that is used.</returns>
        public IActionResult Privacy()
        {
            return View();
        }

        /// <summary>
        /// The Error sender method.
        /// </summary>
        /// <returns>The view of errors.</returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
