﻿namespace MVCWebApp.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Reader model class with roles: form model and MVC ViewModel.
    /// </summary>
    public class ReaderMVC
    {
        /// <summary>
        /// Gets or sets bookId is a primary key in the People table.
        /// </summary>
        [Display(Name = "Person ID")]
        public int PersonId { get; set; }

        /// <summary>
        /// Gets or sets Name is an attribute key in the People table.
        /// </summary>
        [Display(Name = "Person Name")]
        [StringLength(140)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Address is an attribute in the People table.
        /// </summary>
        [Display(Name = "Person Address")]
        [Required]
        [StringLength(115)]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets Email is an attribute in the People table.
        /// </summary>
        [Display(Name = "Person Email")]
        [Required]
        [StringLength(115)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets MotherName is an attribute key in the People table.
        /// </summary>
        public string MotherName { get; set; }

        /// <summary>
        /// Gets or sets BirthDate is an attribute in the Books table.
        /// </summary>
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// Gets or sets BirthPlace is an attribute in the People table. The place where the person was bored.
        /// </summary>
        public string BirthPlace { get; set; }

        /// <summary>
        /// Gets or sets EnterDate is an attribute in the Books table.
        /// </summary>
        public DateTime EnterDate { get; set; }
    }
}