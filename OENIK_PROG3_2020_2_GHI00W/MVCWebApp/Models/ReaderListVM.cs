﻿namespace MVCWebApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The view model of readers.
    /// </summary>
    public class ReaderListVM
    {
        /// <summary>
        /// Gets all readers.
        /// </summary>
        public Collection<ReaderMVC> ReaderList { get; private set; }

        /// <summary>
        /// Gets or sets the selected, edited reader.
        /// </summary>
        public ReaderMVC EditedReader { get; set; }

        /// <summary>
        /// Sets the ReaderList.
        /// </summary>
        /// <param name="list">The list.</param>
        public void SetReaderList(Collection<ReaderMVC> list)
        {
            if (list != null)
            {
                ReaderList = list;
            }
        }
    }
}
