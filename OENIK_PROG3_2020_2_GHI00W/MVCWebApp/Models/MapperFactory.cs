﻿namespace MVCWebApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// Makes the map between the db type and ui type.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Makes the map between the db type and ui type.
        /// </summary>
        /// <returns>The map between to classes.</returns>
        internal static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BookShelf.Data.Entities.People, MVCWebApp.Models.ReaderMVC>().
                    ForMember(dest => dest.PersonId, map => map.MapFrom(src => src.PersonId)).
                    ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name)).
                    ForMember(dest => dest.Address, map => map.MapFrom(src => src.Address)).
                    ForMember(dest => dest.Email, map => map.MapFrom(src => src.Email)).
                    ForMember(dest => dest.MotherName, map => map.MapFrom(src => src.MotherName)).
                    ForMember(dest => dest.EnterDate, map => map.MapFrom(src => src.EnterDate)).
                    ForMember(dest => dest.BirthPlace, map => map.MapFrom(src => src.BirthPlace)).
                    ForMember(dest => dest.BirthDate, map => map.MapFrom(src => src.BirthDate)).
                    ReverseMap();
            });
            return config.CreateMapper();
        }
    }
}
