﻿namespace BookShelf.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Authors class in the Data.Entities.
    /// </summary>
    [Table("authors")]
    public class Authors
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Authors"/> class.
        /// </summary>
        public Authors()
        {
            this.Bks = new HashSet<Books>();
        }

        /// <summary>
        /// Gets or sets AuthorId is a primary key in the Authors table.
        /// </summary>
        [Key]
        public int AuthorId { get; set; }

        /// <summary>
        /// Gets or sets Name is an attribute key in the Authors table.
        /// </summary>
        [MaxLength(140)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets BirthName is an attribute in the Authors table.
        /// </summary>
        [MaxLength(140)]
        public string BirthName { get; set; }

        /// <summary>
        /// Gets or sets BirthYear is an attribute in the Authors table.
        /// </summary>
        public int BirthYear { get; set; }

        /// <summary>
        /// Gets or sets BirthCompany is an attribute in the Authors table.
        /// </summary>
        [MaxLength(115)]
        public string BirthCountry { get; set; }

        /// <summary>
        /// Gets or sets Nationality is an attribute in the Author table.
        /// </summary>
        [MaxLength(110)]
        public string Nationality { get; set; }

        /// <summary>
        /// Gets Bks is a collection for books which are wrote by the actual author.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Books> Bks { get; }

        /// <summary>
        /// It creates the string using datas of an author.
        /// </summary>
        /// <returns>It returns the created string.</returns>
        public override string ToString()
        {
            return $"{AuthorId}. Name: {Name} was born in {BirthYear} in {BirthCountry} as {BirthName}. Nationality: {Nationality}";
        }

        /// <summary>
        /// It tests whether the 2 objects are equal or not.
        /// </summary>
        /// <param name="obj">The object which is equal to this.</param>
        /// <returns>It returns true when the object eguals to this, else false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Books)
            {
                Books other = obj as Books;
                return AuthorId == other.AuthorId;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// It generates a hashcode for the object.
        /// </summary>
        /// <returns>It returns the hashcode.</returns>
        public override int GetHashCode()
        {
            return ((Nationality.Length * 117) + (BirthYear * 3) - (Name.Length * 13 / 3)) / 107;
        }
    }
}
