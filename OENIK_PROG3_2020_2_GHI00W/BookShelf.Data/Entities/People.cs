﻿namespace BookShelf.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Text;

    /// <summary>
    /// People class in the Data.Entity.
    /// </summary>
    [Table("people")]
    public class People
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="People"/> class.
        /// </summary>
        public People()
        {
            this.Lnd = new HashSet<Lends>();
        }

        /// <summary>
        /// Gets or sets BookId is a primary key in the People table.
        /// </summary>
        [Key]
        public int PersonId { get; set; }

        /// <summary>
        /// Gets or sets Name is an attribute key in the People table.
        /// </summary>
        [MaxLength(140)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets MotherName is an attribute key in the People table.
        /// </summary>
        [MaxLength(140)]
        [Required]
        public string MotherName { get; set; }

        /// <summary>
        /// Gets or sets BirthDate is an attribute in the Books table.
        /// </summary>
        [Required]
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// Gets or sets BirthPlace is an attribute in the People table. The place where the person was bored.
        /// </summary>
        [MaxLength(15)]
        [Required]
        public string BirthPlace { get; set; }

        /// <summary>
        /// Gets or sets Address is an attribute in the People table.
        /// </summary>
        [MaxLength(115)]
        [Required]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets Email is an attribute in the People table.
        /// </summary>
        [MaxLength(115)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets EnterDate is an attribute in the Books table.
        /// </summary>
        public DateTime EnterDate { get; set; }

        /// <summary>
        /// Gets LbryN is a reference collection for notes which show us the date of lend.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Lends> Lnd { get; }

        /// <summary>
        /// It creates the string using datas of an author.
        /// </summary>
        /// <returns>It returns the created string.</returns>
        public override string ToString()
        {
            return $"({PersonId}) {Name} was born on {BirthDate.ToString("yyyy/MM/dd", new CultureInfo("en-US"))} in {BirthPlace}. His/her mother is {MotherName} and they live in {Address}. Email: {Email}";
        }

        /// <summary>
        /// It tests whether the 2 objects are equal or not.
        /// </summary>
        /// <param name="obj">The object which is equal to this.</param>
        /// <returns>It returns true when the object eguals to this, else false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is People)
            {
                People other = obj as People;
                return PersonId == other.PersonId;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// It generates a hashcode for the object.
        /// </summary>
        /// <returns>It returns the hashcode.</returns>
        public override int GetHashCode()
        {
            return (Name.Length / 7) + this.BirthDate.DayOfYear - (Address.Length * 13 / 3);
        }
    }
}
