﻿namespace BookShelf.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The DBContext class which makes relation between database, entities.
    /// </summary>
    public partial class BookShelfDBContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BookShelfDBContext"/> class.
        /// </summary>
        public BookShelfDBContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets DBSet for authors.
        /// </summary>
        public virtual DbSet<Authors> Authors { get; set; }

        /// <summary>
        /// Gets or sets DBSet for books.
        /// </summary>
        public virtual DbSet<Books> Books { get; set; }

        /// <summary>
        /// Gets or sets DBSet for people.
        /// </summary>
        public virtual DbSet<People> People { get; set; }

        /// <summary>
        /// Gets or sets DBSet for lends.
        /// </summary>
        public virtual DbSet<Lends> Lends { get; set; }

        /// <summary>
        /// Overriding the OnConfiguring methode from the ancient class.
        /// </summary>
        /// <param name="optionsBuilder"> I overrided the OnConfiguring methode.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null && !optionsBuilder.IsConfigured)
                {
                    optionsBuilder.UseLazyLoadingProxies().UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=|DataDirectory|\BookShelfDatabase.mdf;Integrated Security=True; MultipleActiveResultSets=true");
                }
        }

        /// <summary>
        /// Overriding the OnModelCreating methode from the ancient class.
        /// </summary>
        /// <param name="modelBuilder"> I overrided the OnModelCreating methode.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder != null)
            {
                modelBuilder.Entity<Books>(entity =>
                {
                    entity
                    .HasOne(book => book.Authr)
                    .WithMany(author => author.Bks)
                    .HasForeignKey(book => book.AuthorId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
                });
                modelBuilder.Entity<Lends>(entity =>
                {
                    entity
                    .HasOne(lending => lending.Pple)
                    .WithMany(person => person.Lnd)
                    .HasForeignKey(lending => lending.PersonId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
                });
                modelBuilder.Entity<Lends>(entity =>
                {
                    entity
                    .HasOne(lending => lending.Bok)
                    .WithMany(book => book.Lnd)
                    .HasForeignKey(lending => lending.BookId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
                });

                Authors author1 = new Authors() { AuthorId = 1, BirthCountry = "-", BirthName = "-", BirthYear = 0, Name = "Unknown", Nationality = "-" };
                Authors author2 = new Authors() { AuthorId = 2, BirthCountry = "England", BirthName = "Agatha Mary Clarissa Miller", BirthYear = 1890, Name = "Agatha Christie", Nationality = "british" };
                Authors author3 = new Authors() { AuthorId = 3, BirthCountry = "England", BirthName = "Joanne Rowling", BirthYear = 1965, Name = "J. K. Rowling", Nationality = "british" };
                Authors author4 = new Authors() { AuthorId = 4, BirthCountry = "Hungary", BirthName = "Fekete István", BirthYear = 1890, Name = "Fekete István", Nationality = "hungarian" };
                Authors author5 = new Authors() { AuthorId = 5, BirthCountry = "Romania", BirthName = "Kolozsi Ildikó", BirthYear = 1970, Name = "L. I. Lázár", Nationality = "hungarian" };

                Books book1 = new Books() { BookId = 1, AuthorId = 2, GetDate = Convert.ToDateTime("2006.03.16", CultureInfo.CurrentCulture), Pages = 278, Place = "Budapest", Publisher = "Európa", Title = "Macska a galambok között", Year = 2005 };
                Books book2 = new Books() { BookId = 2, AuthorId = 2, GetDate = Convert.ToDateTime("2015.03.06", CultureInfo.CurrentCulture), Pages = 240, Place = "Budapest", Publisher = "Európa", Title = "A nagy négyes", Year = 2013 };
                Books book3 = new Books() { BookId = 3, AuthorId = 3, GetDate = Convert.ToDateTime("2017.07.16", CultureInfo.CurrentCulture), Pages = 574, Place = "Budapest", Publisher = "GABO", Title = "Átmeneti üresedés", Year = 2012 };
                Books book4 = new Books() { BookId = 4, AuthorId = 3, GetDate = Convert.ToDateTime("2002.03.16", CultureInfo.CurrentCulture), Pages = 674, Place = "Budapest", Publisher = "Animus", Title = "Harry Potter és a tűz serlege", Year = 2000 };
                Books book5 = new Books() { BookId = 5, AuthorId = 4, GetDate = Convert.ToDateTime("2016.07.16", CultureInfo.CurrentCulture), Pages = 182, Place = "Déva", Publisher = "Corvin", Title = "Vuk", Year = 2002 };
                Books book6 = new Books() { BookId = 6, AuthorId = 5, GetDate = Convert.ToDateTime("2018.07.16", CultureInfo.CurrentCulture), Pages = 250, Place = "Marosvásárhely", Publisher = "Móra", Title = "Rejtőző kavicsok I", Year = 2012 };
                Books book7 = new Books() { BookId = 7, AuthorId = 4, GetDate = Convert.ToDateTime("2015.07.16", CultureInfo.CurrentCulture), Pages = 182, Place = "Déva", Publisher = "Corvin", Title = "Bogáncs", Year = 2002 };

                People person1 = new People() { PersonId = 1, Address = "Budapest IX. Rákos utca 3", BirthPlace = "Budapest", MotherName = "Szegény Ibolya", Name = "Nemecsek Ernő", Email = "nemecsekerno@kisbetu-grund.hu", EnterDate = Convert.ToDateTime("2005.03.16", CultureInfo.CurrentCulture), BirthDate = Convert.ToDateTime("1995.03.16", CultureInfo.CurrentCulture) };
                People person2 = new People() { PersonId = 2, Address = "Debrecen, Kollégium utca 13, 133 szoba", BirthPlace = "Sárospatak", MotherName = "Szorgos Ilona", Name = "Nyilas Misi", Email = "nyilasm1892@koll-debrecen.hu", EnterDate = Convert.ToDateTime("2002.07.26", CultureInfo.CurrentCulture), BirthDate = Convert.ToDateTime("1987.11.16", CultureInfo.CurrentCulture) };
                People person3 = new People() { PersonId = 3, Address = "KisDöbrögfalva 3333", BirthPlace = "Döbrög", MotherName = "Lúdas Mami", Name = "Lúdas Matyi", Email = "ludas_matyko@haromszor.hu", EnterDate = Convert.ToDateTime("2015.08.01", CultureInfo.CurrentCulture), BirthDate = Convert.ToDateTime("2000.12.31", CultureInfo.CurrentCulture) };

                Lends lend1 = new Lends() { LendId = 1, PersonId = 1, BookId = 1, LendDate = Convert.ToDateTime("2016.03.16", CultureInfo.CurrentCulture), GivebackDate = Convert.ToDateTime("2016.04.10", CultureInfo.CurrentCulture) };
                Lends lend2 = new Lends() { LendId = 2, PersonId = 1, BookId = 3, LendDate = Convert.ToDateTime("2017.06.06", CultureInfo.CurrentCulture), GivebackDate = Convert.ToDateTime("2017.07.01", CultureInfo.CurrentCulture) };
                Lends lend3 = new Lends() { LendId = 3, PersonId = 2, BookId = 2, LendDate = Convert.ToDateTime("2017.01.12", CultureInfo.CurrentCulture), GivebackDate = Convert.ToDateTime("2017.01.22", CultureInfo.CurrentCulture) };
                Lends lend4 = new Lends() { LendId = 4, PersonId = 2, BookId = 4, LendDate = Convert.ToDateTime("2019.12.05", CultureInfo.CurrentCulture), GivebackDate = Convert.ToDateTime("2019.12.15", CultureInfo.CurrentCulture) };
                Lends lend5 = new Lends() { LendId = 5, PersonId = 3, BookId = 5, LendDate = Convert.ToDateTime("2020.10.29", CultureInfo.CurrentCulture) };
                Lends lend6 = new Lends() { LendId = 6, PersonId = 1, BookId = 2, LendDate = Convert.ToDateTime("2018.01.12", CultureInfo.CurrentCulture), GivebackDate = Convert.ToDateTime("2018.01.22", CultureInfo.CurrentCulture) };
                Lends lend7 = new Lends() { LendId = 7, PersonId = 2, BookId = 4, LendDate = Convert.ToDateTime("2020.11.05", CultureInfo.CurrentCulture) };

                modelBuilder.Entity<Books>().HasData(book1, book2, book3, book4, book5, book6, book7);
                modelBuilder.Entity<Authors>().HasData(author5, author1, author2, author3, author4);
                modelBuilder.Entity<People>().HasData(person1, person2, person3);
                modelBuilder.Entity<Lends>().HasData(lend1, lend2, lend3, lend4, lend5, lend6, lend7);
            }
        }
    }
}
