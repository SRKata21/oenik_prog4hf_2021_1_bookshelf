﻿namespace WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Relation between ui and api.
    /// </summary>
    public class MainLogic : ILogic
    {
        private string url = "http://localhost:60047/ReaderApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// Gets all readers.
        /// </summary>
        /// <returns>List of readers.</returns>
        public IList<ReaderVM> ApiGetReaders()
        {
            Uri u = new Uri(url + "all");
            string json = client.GetStringAsync(u).Result;
            var list = JsonSerializer.Deserialize<List<ReaderVM>>(json, jsonOptions);
            /*Collection<ReaderVM> rlist = new Collection<ReaderVM>();
            foreach (var item in list)
            {
                rlist.Add(item);
            }

            return rlist;*/

            return list;
        }

        /// <summary>
        /// Delete a reader.
        /// </summary>
        /// <param name="reader">Reader that should be deleted.</param>
        public void ApiDelReader(ReaderVM reader)
        {
            bool success = false;
            if (reader != null)
            {
                Uri u = new Uri(url + "del/" + reader.PersonId.ToString(CultureInfo.CurrentCulture));
                string json = client.GetStringAsync(u).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            SendMessage(success);
        }

        /// <summary>
        /// To edit or create a new reader.
        /// </summary>
        /// <param name="reader">The reader that should be modified.</param>
        /// <param name="editorFunc">True if modify, false if new reader.</param>
        public void EditReader(ReaderVM reader, Func<ReaderVM, bool> editorFunc)
        {
            ReaderVM clone = new ReaderVM();
            if (reader != null)
            {
                clone.Copy(reader);
            }

            bool? success = editorFunc?.Invoke(clone);
            if (success == true)
            {
                if (reader != null)
                {
                    success = ApiEditReader(clone, true);
                }
                else
                {
                    success = ApiEditReader(clone, false);
                }
            }

            SendMessage(success == true);
        }

        private static void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "ReaderResult");
        }

        private bool ApiEditReader(ReaderVM reader, bool isEditing)
        {
            if (reader == null)
            {
                return false;
            }

            Uri myUrl = new Uri(url + (isEditing ? "mod" : "add"));
            Dictionary<string, string> postData = new Dictionary<string, string>();
            postData.Add("personid", reader.PersonId.ToString(CultureInfo.CurrentCulture));
            postData.Add("name", reader.Name);
            postData.Add("address", reader.Address);
            postData.Add("email", reader.Email);

            // postData.Add("motherame", "def");
            // postData.Add("birthPlace", "def");
            using (var formUrl = new FormUrlEncodedContent(postData))
            {
                // formUrl.Dispose();
                string json = client.PostAsync(myUrl, formUrl).Result.Content.ReadAsStringAsync().Result;
                JsonDocument doc = JsonDocument.Parse(json);
                return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }
        }
    }
}
