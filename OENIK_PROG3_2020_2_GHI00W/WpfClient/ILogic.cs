﻿namespace WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for logic.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Gets all readers.
        /// </summary>
        /// <returns>List of readers.</returns>
        public IList<ReaderVM> ApiGetReaders();

        /// <summary>
        /// Delete a reader.
        /// </summary>
        /// <param name="reader">Reader that should be deleted.</param>
        public void ApiDelReader(ReaderVM reader);

        /// <summary>
        /// To edit or create a new reader.
        /// </summary>
        /// <param name="reader">The reader that should be modified.</param>
        /// <param name="editorFunc">True if modify, false if new reader.</param>
        public void EditReader(ReaderVM reader, Func<ReaderVM, bool> editorFunc);
    }
}
