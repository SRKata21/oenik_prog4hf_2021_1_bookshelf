﻿namespace WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Main View Model.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private ILogic logic;
        private ReaderVM selectedReader;
        private ObservableCollection<ReaderVM> allReader;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="logic">The logic to operation.</param>
        public MainVM(ILogic logic)
        {
            this.logic = logic;
            LoadCmd = new RelayCommand(() => AllReader = new ObservableCollection<ReaderVM>(this.logic.ApiGetReaders()));
            DelCmd = new RelayCommand(() => this.logic.ApiDelReader(SelectedReader));
            AddCmd = new RelayCommand(() => this.logic.EditReader(null, EditorFunc));
            ModCmd = new RelayCommand(() => this.logic.EditReader(SelectedReader, EditorFunc));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<ILogic>())
        {
        }

        /// <summary>
        /// Gets or sets the selected reader in the window.
        /// </summary>
        public ReaderVM SelectedReader
        {
            get { return selectedReader; }
            set { Set(ref selectedReader, value); }
        }

        /// <summary>
        /// Gets or sets the list of the readers in the window.
        /// </summary>
        public ObservableCollection<ReaderVM> AllReader
        {
            get { return allReader; }
            set { Set(ref allReader, value); }
        }

        /// <summary>
        /// Gets or sets that the reader should be modified or added.
        /// </summary>
        public Func<ReaderVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets command for add new reader.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets command for delete the reader.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets command for edit the reader.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets command for load the list of the readers.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
