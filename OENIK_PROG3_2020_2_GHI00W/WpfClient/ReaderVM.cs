﻿namespace WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShelf.Data.Entities;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Reader class for DTO and VM.
    /// </summary>
    public class ReaderVM : ObservableObject
    {
        private int personId;
        private string name;
        private string address;
        private string email;
        /*private string motherName;
        private DateTime birthDate;
        private string birthPlace;
        private DateTime enterDate;*/

        /// <summary>
        /// Gets or sets BookId is a primary key in the People table.
        /// </summary>
        public int PersonId
        {
            get { return personId; }
            set { Set(ref personId, value); }
        }

        /// <summary>
        /// Gets or sets Name is an attribute key in the People table.
        /// </summary>
        public string Name
        {
            get { return name; }
            set { Set(ref name, value); }
        }

        ///// <summary>
        ///// Gets or sets MotherName is an attribute key in the People table.
        ///// </summary>
        // public string MotherName
        // {
        //    get { return motherName; }
        //    set { Set(ref motherName, value); }
        // }

        ///// <summary>
        ///// Gets or sets BirthDate is an attribute in the Books table.
        ///// </summary>
        // public DateTime BirthDate
        // {
        //    get { return birthDate; }
        //    set { Set(ref birthDate, value); }
        // }

        ///// <summary>
        ///// Gets or sets BirthPlace is an attribute in the People table. The place where the person was bored.
        ///// </summary>
        // public string BirthPlace
        // {
        //    get { return birthPlace; }
        //    set { Set(ref birthPlace, value); }
        // }

        /// <summary>
        /// Gets or sets Address is an attribute in the People table.
        /// </summary>
        public string Address
        {
            get { return address; }
            set { Set(ref address, value); }
        }

        /// <summary>
        /// Gets or sets Email is an attribute in the People table.
        /// </summary>
        public string Email
        {
            get { return email; }
            set { Set(ref email, value); }
        }

        ///// <summary>
        ///// Gets or sets EnterDate is an attribute in the Books table.
        ///// </summary>
        // public DateTime EnterDate
        // {
        //    get { return enterDate; }
        //    set { Set(ref enterDate, value); }
        // }

        /// <summary>
        /// It makes a copy about the other object.
        /// </summary>
        /// <param name="other">The object that should be copied.</param>
        public void Copy(ReaderVM other)
        {
            /*this.GetType().GetProperties().ToList().ForEach(
                property => property.SetValue(this, property.GetValue(other)));*/
            if (other == null)
            {
                return;
            }

            this.PersonId = other.PersonId;
            this.Address = other.Address;
            this.Email = other.Email;
            this.Name = other.Name;

            // this.BirthDate = other.BirthDate;
            // this.BirthPlace = other.BirthPlace;
            // this.EnterDate = other.EnterDate;
            // this.MotherName = other.MotherName;
        }

        /// <summary>
        /// It converts ReadersW to People.
        /// </summary>
        /// <returns>It returns a db compatible entity.</returns>
        public People Convert()
        {
            People person = new People();
            person.Name = Name;
            person.Address = Address;
            person.Email = Email;
            person.MotherName = "Default";
            person.BirthDate = DateTime.Now;
            person.BirthPlace = "Default";
            person.EnterDate = DateTime.Now;
            /*person.MotherName = MotherName;
            person.BirthDate = BirthDate;
            person.BirthPlace = BirthPlace;
            person.EnterDate = EnterDate;*/
            return person;
        }

        /// <summary>
        /// It creates the string using datas of an author.
        /// </summary>
        /// <returns>It returns the created string.</returns>
        public override string ToString()
        {
            return $"({PersonId}) {Name} - {Address} - Email: {Email}";
        }
    }
}
