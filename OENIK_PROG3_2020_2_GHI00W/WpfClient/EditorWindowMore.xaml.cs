﻿namespace WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for EditorWindowMore.xaml.
    /// </summary>
    public partial class EditorWindowMore : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindowMore"/> class.
        /// </summary>
        public EditorWindowMore()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindowMore"/> class.
        /// </summary>
        /// <param name="reader">The reader instance.</param>
        public EditorWindowMore(ReaderVM reader)
            : this()
        {
            this.DataContext = reader;
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
