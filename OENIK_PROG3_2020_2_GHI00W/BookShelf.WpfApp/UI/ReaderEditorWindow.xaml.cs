﻿namespace BookShelf.WpfApp.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BookShelf.WpfApp.Data;
    using BookShelf.WpfApp.VM;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Interaction logic for ReaderEditorWindow.xaml.
    /// </summary>
    public partial class ReaderEditorWindow : Window
    {
        private ReaderEditorViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderEditorWindow"/> class.
        /// </summary>
        public ReaderEditorWindow()
        {
            InitializeComponent();

            vm = FindResource("VM") as ReaderEditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderEditorWindow"/> class.
        /// </summary>
        /// <param name="reader">The edited instance.</param>
        public ReaderEditorWindow(ReadersW reader)
            : this()
        {
            vm.Reader = reader;
        }

        /// <summary>
        /// Gets the reader of window.
        /// </summary>
        public ReadersW Reader { get => vm.Reader; }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
