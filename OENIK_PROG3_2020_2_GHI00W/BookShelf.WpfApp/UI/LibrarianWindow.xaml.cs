﻿namespace BookShelf.WpfApp.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BookShelf.WpfApp.Data;
    using BookShelf.WpfApp.VM;

    /// <summary>
    /// Interaction logic for LibrarianWindow.xaml.
    /// </summary>
    public partial class LibrarianWindow : Window
    {
        private readonly LibrarianViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="LibrarianWindow"/> class.
        /// </summary>
        public LibrarianWindow()
        {
            InitializeComponent();

            vm = FindResource("VM") as LibrarianViewModel;
        }

        /// <summary>
        /// Gets the Lend of window.
        /// </summary>
        public LendsW Lend { get => vm.SelectedLend; }

        private void NewLendClick(object sender, RoutedEventArgs e)
        {
            LendEditorWindow leW = new LendEditorWindow();
            leW.ShowDialog();
        }

        private void GiveBackClick(object sender, RoutedEventArgs e)
        {
            vm.SelectedLend.GivebackDate = DateTime.Now;
        }
    }
}
