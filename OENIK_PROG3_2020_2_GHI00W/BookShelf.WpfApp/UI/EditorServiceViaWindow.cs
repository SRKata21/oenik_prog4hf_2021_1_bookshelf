﻿namespace BookShelf.WpfApp.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShelf.WpfApp.BL;
    using BookShelf.WpfApp.Data;

    /// <summary>
    /// Editor service via window.
    /// </summary>
    public class EditorServiceViaWindow : IEditorService
    {
        /// <summary>
        /// It shows if editing is successed or not.
        /// </summary>
        /// <param name="author"> The obj that has to be edited.</param>
        /// <returns>It returns true if editing is true, else false.</returns>
        public bool EditAuthor(AuthorsW author)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// It shows if editing is successed or not.
        /// </summary>
        /// <param name="book"> The obj that has to be edited.</param>
        /// <returns>It returns true if editing is true, else false.</returns>
        public bool EditBook(BooksW book)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// It shows if editing is successed or not.
        /// </summary>
        /// <param name="lend"> The obj that has to be edited.</param>
        /// <returns>It returns true if editing is true, else false.</returns>
        public bool EditLend(LendsW lend)
        {
            LibrarianWindow win = new LibrarianWindow();
            return win.ShowDialog() ?? false;
        }

        /// <summary>
        /// It shows if editing is successed or not.
        /// </summary>
        /// <param name="reader"> The obj that has to be edited.</param>
        /// <returns>It returns true if editing is true, else false.</returns>
        public bool EditNewReader(ReadersW reader)
        {
            ReaderEditorWindow win = new ReaderEditorWindow(reader);
            return win.ShowDialog() ?? false;
        }

        /// <summary>
        /// It shows if editing is successed or not.
        /// </summary>
        /// <param name="reader"> The obj that has to be edited.</param>
        /// <returns>It returns true if editing is true, else false.</returns>
        public bool EditReader(ReadersW reader)
        {
            ReaderDataEditorWindow win = new ReaderDataEditorWindow(reader);
            return win.ShowDialog() ?? false;
        }
    }
}
