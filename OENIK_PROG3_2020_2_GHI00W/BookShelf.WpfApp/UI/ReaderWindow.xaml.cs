﻿namespace BookShelf.WpfApp.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for ReaderWindow.xaml.
    /// </summary>
    public partial class ReaderWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderWindow"/> class.
        /// </summary>
        public ReaderWindow()
        {
            InitializeComponent();
        }
    }
}
