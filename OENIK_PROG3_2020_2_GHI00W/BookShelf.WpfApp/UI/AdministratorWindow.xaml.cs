﻿namespace BookShelf.WpfApp.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BookShelf.WpfApp.Data;
    using BookShelf.WpfApp.VM;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Interaction logic for AdministratorWindow.xaml.
    /// </summary>
    public partial class AdministratorWindow : Window
    {
        private ReaderViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdministratorWindow"/> class.
        /// </summary>
        public AdministratorWindow()
        {
            InitializeComponent();

            vm = FindResource("VM") as ReaderViewModel;
        }

        /// <summary>
        /// Gets the selected reader.
        /// </summary>
        public ReadersW Reader { get => vm.SelectedReader; }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            vm = FindResource("VM") as ReaderViewModel;
            Messenger.Default.Register<string>(this, "LogicResult", msg => { MessageBox.Show(msg); });
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }
    }
}
