﻿namespace BookShelf.WpfApp.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BookShelf.WpfApp.Data;
    using BookShelf.WpfApp.VM;

    /// <summary>
    /// Interaction logic for ReaderDataEditorWindow.xaml.
    /// </summary>
    public partial class ReaderDataEditorWindow : Window
    {
        private ReaderEditorViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderDataEditorWindow"/> class.
        /// </summary>
        public ReaderDataEditorWindow()
        {
            InitializeComponent();

            vm = FindResource("VM") as ReaderEditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderDataEditorWindow"/> class.
        /// </summary>
        /// <param name="reader">The edited instance.</param>
        public ReaderDataEditorWindow(ReadersW reader)
            : this()
        {
            vm.Reader = reader;
        }

        /// <summary>
        /// Gets the reader of window.
        /// </summary>
        public ReadersW Reader { get => vm.Reader; }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
