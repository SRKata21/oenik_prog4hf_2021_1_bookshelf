﻿namespace BookShelf.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShelf.Data.Entities;
    using BookShelf.WpfApp.Data;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// The real logic class hat implements ILogic interface (Crud methods).
    /// </summary>
    public class LendLogic : ILogic<LendsW>
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private Factory factory = new Factory(new BookShelfDBContext());

        /// <summary>
        /// Initializes a new instance of the <see cref="LendLogic"/> class.
        /// </summary>
        /// <param name="editorService"> It helps in editing the object. </param>
        /// <param name="messengerService"> An aspect which helps in communication. </param>
        public LendLogic(IEditorService editorService, IMessenger messengerService)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
        }

        /// <summary>
        /// It adds a new object to the list.
        /// </summary>
        /// <param name="list">The list where will be the object.</param>
        public void Add(IList<LendsW> list)
        {
            LendsW entity = new LendsW();
            if (editorService.EditLend(entity) == true)
            {
                if (list == null)
                {
                    list = new List<LendsW>();
                }

                list.Add(entity);
                factory.Lbr.InsertLend(entity.Convert());
                messengerService.Send("Added successfully", "LogicResult");
            }
            else
            {
                messengerService.Send("Added cancelled", "LogicResult");
            }
        }

        /// <summary>
        /// It deletes a new object from the list.
        /// </summary>
        /// <param name="list">The list where is the object.</param>
        /// <param name="obj">It has to be deleted.</param>
        public void Delete(IList<LendsW> list, LendsW obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// It returns the list of the objects.
        /// </summary>
        /// <returns>It is the list of the objects.</returns>
        public IList<LendsW> GetAll()
        {
            IList<Lends> lendDB = factory.Lbr.GetAllLends();
            IList<LendsW> lendW = new List<LendsW>();
            foreach (Lends item in lendDB)
            {
                LendsW lend = new LendsW();
                lend.LendId = item.LendId;
                lend.PersonId = item.PersonId;
                lend.BookId = item.LendId;
                lend.LendDate = item.LendDate;
                lend.GivebackDate = item.GivebackDate;
                lendW.Add(lend);
            }

            return lendW;
        }

        /// <summary>
        /// It modifies an object.
        /// </summary>
        /// <param name="list">The list where the object exists.</param>
        /// <param name="obj"> It has to be modified.</param>
        public void Modify(IList<LendsW> list, LendsW obj)
        {
            if (obj == null)
            {
                messengerService.Send("Delete failed", "LogicResult");
                return;
            }
            else
            {
                LendsW clone = new LendsW();
                clone.Copy(obj);
                if (editorService.EditLend(clone) == true)
                {
                    obj.Copy(clone);
                    factory.Lbr.ChangeLendGiveBackDate(clone.LendId, clone.GivebackDate);
                    messengerService.Send("Modyfied successfully", "LogicResult");
                }
                else
                {
                    messengerService.Send("Modyfy cancelled", "LogicResult");
                }
            }
        }
    }
}
