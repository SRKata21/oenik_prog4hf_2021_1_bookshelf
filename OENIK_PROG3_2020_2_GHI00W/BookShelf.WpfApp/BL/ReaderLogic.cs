﻿namespace BookShelf.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShelf.Data.Entities;
    using BookShelf.WpfApp.Data;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// The real logic class that implements ILogic interface (Crud methods).
    /// </summary>
    public class ReaderLogic : ILogic<ReadersW>
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private Factory factory = new Factory(new BookShelfDBContext());

        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderLogic"/> class.
        /// </summary>
        /// <param name="editorService"> It helps in editing the object. </param>
        /// <param name="messengerService"> An aspect which helps in communication. </param>
        public ReaderLogic(IEditorService editorService, IMessenger messengerService)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
        }

        /// <summary>
        /// It adds a new object to the list.
        /// </summary>
        /// <param name="list">The list where will be the object.</param>
        public void Add(IList<ReadersW> list)
        {
            ReadersW entity = new ReadersW();
            if (editorService.EditNewReader(entity) == true)
            {
                if (list == null)
                {
                    list = new List<ReadersW>();
                }

                factory.Adm.InsertPerson(entity.Convert());
                list.ToList().ForEach(x => list.Remove(x));
                GetAll().ToList().ForEach(x => list.Add(x));
                /*foreach (ReadersW item in GetAll())
                {
                    list.Add(item);
                }*/

                messengerService.Send("Added successfully", "LogicResult");
            }
            else
            {
                messengerService.Send("Added cancelled", "LogicResult");
            }
        }

        /// <summary>
        /// It deletes a new object from the list.
        /// </summary>
        /// <param name="list">The list where is the object.</param>
        /// <param name="obj">It has to be deleted.</param>
        public void Delete(IList<ReadersW> list, ReadersW obj)
        {
            if (list != null && obj != null && list.Remove(obj))
            {
                factory.Adm.RemovePersonByKey(obj.PersonId);
                messengerService.Send("Deleted successfully", "LogicResult");
            }
            else
            {
                messengerService.Send("Deleted cancelled", "LogicResult");
            }
        }

        /// <summary>
        /// It returns the list of the objects.
        /// </summary>
        /// <returns>It is the list of the objects.</returns>
        public IList<ReadersW> GetAll()
        {
            IList<People> entityDB = factory.Adm.GetAllPeople();
            IList<ReadersW> entityW = new List<ReadersW>();
            foreach (People item in entityDB)
            {
                ReadersW person = new ReadersW();
                person.PersonId = item.PersonId;
                person.Name = item.Name;
                person.MotherName = item.MotherName;
                person.Address = item.Address;
                person.BirthDate = item.BirthDate;
                person.BirthPlace = item.BirthPlace;
                person.Email = item.Email;
                person.EnterDate = item.EnterDate;
                entityW.Add(person);
            }

            return entityW;
        }

        /// <summary>
        /// It modifies an object.
        /// </summary>
        /// <param name="list">The list where the obj exists.</param>
        /// <param name="obj"> It has to be modified.</param>
        public void Modify(IList<ReadersW> list, ReadersW obj)
        {
            if (obj == null || list == null)
            {
                messengerService.Send("Modify failed", "LogicResult");
                return;
            }

            ReadersW clone = new ReadersW();
            clone.Copy(obj);
            if (editorService.EditReader(clone) == true)
            {
                list.Remove(obj);
                obj.Copy(clone);
                list.Add(obj);
                factory.Rdr.ChangePersonAddress(clone.PersonId, clone.Address);
                factory.Rdr.ChangePersonEmail(clone.PersonId, clone.Email);
                messengerService.Send("Modified successfully", "LogicResult");
            }
            else
            {
                messengerService.Send("Modify cancelled", "LogicResult");
            }
        }
    }
}
