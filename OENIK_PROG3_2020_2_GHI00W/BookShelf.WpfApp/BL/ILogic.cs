﻿namespace BookShelf.WpfApp.BL
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// There are CRUD methodes.
    /// </summary>
    /// <typeparam name="T">The Type of the item.</typeparam>
    public interface ILogic<T>
        where T : class
    {
        /// <summary>
        /// It adds a new object to the list.
        /// </summary>
        /// <param name="list">The list where will be the object.</param>
        void Add(IList<T> list);

        /// <summary>
        /// It modifies an object.
        /// </summary>
        /// <param name="list">The list where the object exists.</param>
        /// <param name="obj"> It has to be modified.</param>
        void Modify(IList<T> list, T obj);

        /// <summary>
        /// It deletes a new object from the list.
        /// </summary>
        /// <param name="list">The list where is the object.</param>
        /// <param name="obj">It has to be deleted.</param>
        void Delete(IList<T> list, T obj);

        /// <summary>
        /// It returns the list of the objects.
        /// </summary>
        /// <returns>It is the list of the objects.</returns>
        IList<T> GetAll();
    }
}
