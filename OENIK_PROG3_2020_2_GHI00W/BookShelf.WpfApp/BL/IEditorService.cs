﻿namespace BookShelf.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShelf.WpfApp.Data;

    /// <summary>
    /// It makes object editing without dependency to UI.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Edit method.
        /// </summary>
        /// <param name="lend"> It is the object that have to be edited. </param>
        /// <returns> It represents that editing was success or not. </returns>
        bool EditLend(LendsW lend);

        /// <summary>
        /// Edit method.
        /// </summary>
        /// <param name="book"> It is the object that have to be edited. </param>
        /// <returns> It represents that editing was success or not. </returns>
        bool EditBook(BooksW book);

        /// <summary>
        /// Edit method.
        /// </summary>
        /// <param name="author"> It is the object that have to be edited. </param>
        /// <returns> It represents that editing was success or not. </returns>
        bool EditAuthor(AuthorsW author);

        /// <summary>
        /// Edit method.
        /// </summary>
        /// <param name="reader"> It is the object that have to be edited. </param>
        /// <returns> It represents that editing was success or not. </returns>
        bool EditReader(ReadersW reader);

        /// <summary>
        /// Edit method.
        /// </summary>
        /// <param name="reader"> It is the object that have to be edited. </param>
        /// <returns> It represents that editing was success or not. </returns>
        bool EditNewReader(ReadersW reader);
    }
}
