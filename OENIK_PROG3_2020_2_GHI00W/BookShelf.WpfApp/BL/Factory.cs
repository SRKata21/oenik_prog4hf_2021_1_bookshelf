﻿namespace BookShelf.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BookShelf.Data.Entities;
    using BookShelf.Logic;
    using BookShelf.Repository;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Factory creats some object manager.
    /// </summary>
    internal class Factory
    {
        /// <summary>
        /// Manages the database.
        /// </summary>
        private BookShelfDBContext bsDbCtx;

        /// <summary>
        /// The repository class for book.
        /// </summary>
        private BookManager bMngr;

        /// <summary>
        /// The repository class for author.
        /// </summary>
        private AuthorManager aMngr;

        /// <summary>
        /// The repository class for person.
        /// </summary>
        private PeopleManager pMngr;

        /// <summary>
        /// The repository class for lend.
        /// </summary>
        private LendManager lMngr;

        /// <summary>
        /// Initializes a new instance of the <see cref="Factory"/> class.
        /// </summary>
        /// <param name="ctx">The given DbContext for the initialization.</param>
        public Factory(BookShelfDBContext ctx)
        {
            this.bsDbCtx = ctx;

            this.bMngr = new BookManager(this.bsDbCtx);
            this.aMngr = new AuthorManager(this.bsDbCtx);
            this.pMngr = new PeopleManager(this.bsDbCtx);
            this.lMngr = new LendManager(this.bsDbCtx);

            this.Lbr = new LibrarianUser(this.bMngr, this.aMngr, this.pMngr, this.lMngr);
            this.Rdr = new ReaderUser(this.bMngr, this.aMngr, this.pMngr, this.lMngr);
            this.Adm = new AdministratorUser(this.bMngr, this.aMngr, this.pMngr, this.lMngr);
        }

        /// <summary>
        /// Gets the logic class for the work of the librarian.
        /// </summary>
        public LibrarianUser Lbr { get; }

        /// <summary>
        /// Gets the logic class to get datas.
        /// </summary>
        public ReaderUser Rdr { get; }

        /// <summary>
        /// Gets the logic class to the work of the Administrator.
        /// </summary>
        public AdministratorUser Adm { get; }
    }
}