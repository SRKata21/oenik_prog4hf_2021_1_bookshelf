﻿namespace BookShelf.WpfApp.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Documents;
    using System.Windows.Input;
    using BookShelf.Data.Entities;
    using BookShelf.WpfApp.BL;
    using BookShelf.WpfApp.Data;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Lend Editor Window view model.
    /// </summary>
    public class LendEditorViewModel : ViewModelBase
    {
        private ILogic<LendsW> logic;
        private ObservableCollection<LendsW> allLend;
        private LendsW lend;

        /// <summary>
        /// Initializes a new instance of the <see cref="LendEditorViewModel"/> class.
        /// </summary>
        /// <param name="logic">The logic instance.</param>
        public LendEditorViewModel(ILogic<LendsW> logic)
        {
            this.logic = logic;
            /*person = new ObservableCollection<ReadersW>();
            foreach (People item in logic?.GetAll())
            {
                ReadersW temp = new ReadersW();
                temp.Address = item.Address;
                temp.BirthDate = item.BirthDate;
                temp.BirthPlace = temp.BirthPlace;
                temp.Email = item.Email;
                temp.EnterDate = item.EnterDate;
                temp.MotherName = item.MotherName;
                temp.Name = item.Name;
                temp.PersonId = item.PersonId;
                person.Add(temp);
            }

            book = new ObservableCollection<BooksW>();
            foreach (Books item in logic.GetAll())
            {
                BooksW temp = new BooksW();
                temp.AuthorId = item.AuthorId;
                temp.BookId = item.BookId;
                temp.Pages = item.Pages;
                temp.Place = item.Place;
                temp.Publisher = item.Publisher;
                temp.Title = item.Title;
                temp.Year = item.Year;
                book.Add(temp);
            }
            */
            allLend = new ObservableCollection<LendsW>();
            foreach (var item in logic?.GetAll())
            {
                allLend.Add(item as LendsW);
            }

            SaveCom = new RelayCommand(() => this.logic.Add(allLend));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LendEditorViewModel"/> class.
        /// </summary>
        public LendEditorViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<ILogic<LendsW>>())
        {
        }

        /// <summary>
        /// Gets or sets the lend property.
        /// </summary>
        public LendsW Lend
        {
            get { return lend; }
            set { Set(ref lend, value); }
        }

        /// <summary>
        /// Gets saving.
        /// </summary>
        public ICommand SaveCom { get; private set; }

        /// <summary>
        /// Gets not saving.
        /// </summary>
        public ICommand CancelCom { get; private set; }
    }
}
