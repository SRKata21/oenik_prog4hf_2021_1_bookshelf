﻿namespace BookShelf.WpfApp.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using BookShelf.Data.Entities;
    using BookShelf.WpfApp.BL;
    using BookShelf.WpfApp.Data;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Relation between UI and BL.
    /// </summary>
    public class ReaderViewModel : ViewModelBase
    {
        private ILogic<ReadersW> logic;
        private ReadersW selectedReader;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderViewModel"/> class.
        /// </summary>
        /// <param name="logic">The logic entity that makes relation by dependency injection.</param>
        public ReaderViewModel(ILogic<ReadersW> logic)
        {
            this.logic = logic;

            FillList();

            // AllLends = logic.GetAll().Select(l => new LendsW(l));
            if (IsInDesignMode)
            {
                ReadersW person = new ReadersW();
                person.Name = "Proba";
                person.MotherName = "Proba";
                person.Address = "Proba";
                person.BirthDate = DateTime.Now;
                person.BirthPlace = "Proba";
                person.Email = "Proba";
                person.EnterDate = DateTime.Now;
                AllReader.Add(person);
            }

            SelectedReader = AllReader.FirstOrDefault();
            ModifyComm = new RelayCommand(() => this.logic.Modify(AllReader, SelectedReader));
            DeleteComm = new RelayCommand(() => this.logic.Delete(AllReader, SelectedReader));
            NewComm = new RelayCommand(() => this.logic.Add(AllReader));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderViewModel"/> class.
        /// </summary>
        public ReaderViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<ILogic<ReadersW>>())
        {
        }

        /// <summary>
        /// Gets lends in the list.
        /// </summary>
        public ObservableCollection<ReadersW> AllReader
        {
            get; private set;
        }

        /// <summary>
        /// Gets or sets the selected item from the window.
        /// </summary>
        public ReadersW SelectedReader
        {
            get { return selectedReader; }
            set { Set(ref selectedReader, value); }
        }

        /// <summary>
        /// Gets the command that changes the givebackdate.
        /// </summary>
        public ICommand ModifyComm { get; private set; }

        /// <summary>
        /// Gets the command that adds the new lend.
        /// </summary>
        public ICommand NewComm { get; private set; }

        /// <summary>
        /// Gets the command that removes the lend.
        /// </summary>
        public ICommand DeleteComm { get; private set; }

        /// <summary>
        /// Refresh the list.
        /// </summary>
        public void FillList()
        {
            AllReader = new ObservableCollection<ReadersW>();
            foreach (ReadersW item in logic?.GetAll())
            {
                AllReader.Add(item);
            }
        }
    }
}
