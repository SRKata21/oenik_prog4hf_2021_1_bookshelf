﻿namespace BookShelf.WpfApp.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using BookShelf.WpfApp.BL;
    using BookShelf.WpfApp.Data;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// REader Editor Window view model.
    /// </summary>
    public class ReaderEditorViewModel : ViewModelBase
    {
        private ILogic<ReadersW> logic;
        private ObservableCollection<ReadersW> allReader;
        private ReadersW reader;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderEditorViewModel"/> class.
        /// </summary>
        /// <param name="logic">The logic instance.</param>
        public ReaderEditorViewModel(ILogic<ReadersW> logic)
        {
            this.logic = logic;

            if (IsInDesignMode)
            {
                reader = new ReadersW();
                reader.Name = "Proba";
                reader.MotherName = "Proba";
                reader.Address = "Proba";
                reader.BirthDate = DateTime.Now;
                reader.BirthPlace = "Proba";
                reader.Email = "Proba";
                reader.EnterDate = DateTime.Now;
            }

            allReader = new ObservableCollection<ReadersW>();
            foreach (ReadersW item in logic?.GetAll())
            {
                allReader.Add(item);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderEditorViewModel"/> class.
        /// </summary>
        public ReaderEditorViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<ILogic<ReadersW>>())
        {
        }

        /// <summary>
        /// Gets or sets the lend property.
        /// </summary>
        public ReadersW Reader
        {
            get { return reader; }
            set { Set(ref reader, value); }
        }
    }
}