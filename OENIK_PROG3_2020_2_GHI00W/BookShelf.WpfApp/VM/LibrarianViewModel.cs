﻿namespace BookShelf.WpfApp.VM
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Documents;
    using System.Windows.Input;
    using BookShelf.WpfApp.BL;
    using BookShelf.WpfApp.Data;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Relation between UI and BL.
    /// </summary>
    public class LibrarianViewModel : ViewModelBase
    {
        private ILogic<LendsW> logic;
        private LendsW selectedLend;

        /// <summary>
        /// Initializes a new instance of the <see cref="LibrarianViewModel"/> class.
        /// </summary>
        /// <param name="logic">The logic entity that makes relation by dependency injection.</param>
        public LibrarianViewModel(ILogic<LendsW> logic)
        {
            this.logic = logic;
            AllLends = new ObservableCollection<LendsW>();
            foreach (LendsW item in logic?.GetAll())
            {
                AllLends.Add(item);
            }

            // AllLends = logic.GetAll().Select(l => new LendsW(l));
            if (logic == null)
            {
                LendsW lend = new LendsW();
                lend.LendId = 0;
                lend.BookId = 1;
                lend.PersonId = 2;
                lend.LendDate = DateTime.Now;
                AllLends.Add(lend);
            }

            SelectedLend = AllLends.FirstOrDefault();
            GiveBComm = new RelayCommand(() => this.logic.Modify(AllLends, SelectedLend));
            NewComm = new RelayCommand(() => this.logic.Add(AllLends));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LibrarianViewModel"/> class.
        /// </summary>
        public LibrarianViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<ILogic<LendsW>>())
        {
        }

        /// <summary>
        /// Gets lends in the list.
        /// </summary>
        public ObservableCollection<LendsW> AllLends { get; private set; }

        /// <summary>
        /// Gets or sets the selected item from the window.
        /// </summary>
        public LendsW SelectedLend
        {
            get { return selectedLend; } set { Set(ref selectedLend, value); }
        }

        /// <summary>
        /// Gets the command that changes the givebackdate.
        /// </summary>
        public ICommand GiveBComm { get; private set; }

        /// <summary>
        /// Gets the command that adds the new lend.
        /// </summary>
        public ICommand NewComm { get; private set; }
    }
}
