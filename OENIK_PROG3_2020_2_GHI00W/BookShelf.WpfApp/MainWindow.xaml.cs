﻿namespace BookShelf.WpfApp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using BookShelf.WpfApp.UI;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// MainVindow xaml.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        private void LibrarianClick(object sender, RoutedEventArgs e)
        {
            LibrarianWindow librarianWindow = new LibrarianWindow();
            librarianWindow.Show();
            this.Close();
        }

        private void AdministratorClick(object sender, RoutedEventArgs e)
        {
            AdministratorWindow admWindow = new AdministratorWindow();
            admWindow.Show();
            this.Close();
        }
    }
}
