﻿namespace BookShelf.WpfApp.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShelf.Data.Entities;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// The author class in WPF App.
    /// </summary>
    public class AuthorsW : ObservableObject
    {
        private int authorId;
        private string name;
        private string birthName;
        private int birthYear;
        private string birthCountry;
        private string nationality;

        /// <summary>
        /// Gets or sets AuthorId is a primary key in the Authors table.
        /// </summary>
        public int AuthorId
        {
            get { return authorId; }
            set { Set(ref authorId, value); }
        }

            /// <summary>
            /// Gets or sets Name is an attribute key in the Authors table.
            /// </summary>
        public string Name
        {
            get { return name; }
            set { Set(ref name, value); }
        }

        /// <summary>
        /// Gets or sets BirthName is an attribute in the Authors table.
        /// </summary>
        public string BirthName
        {
            get { return birthName; }
            set { Set(ref birthName, value); }
        }

        /// <summary>
        /// Gets or sets BirthYear is an attribute in the Authors table.
        /// </summary>
        public int BirthYear
        {
            get { return birthYear; }
            set { Set(ref birthYear, value); }
        }

        /// <summary>
        /// Gets or sets BirthCompany is an attribute in the Authors table.
        /// </summary>
        public string BirthCountry
        {
            get { return birthCountry; }
            set { Set(ref birthCountry, value); }
        }

        /// <summary>
        /// Gets or sets Nationality is an attribute in the Author table.
        /// </summary>
        public string Nationality
        {
            get { return nationality; }
            set { Set(ref nationality, value); }
        }

        /// <summary>
        /// It makes a copy about the other object.
        /// </summary>
        /// <param name="other">The object that should be copied.</param>
        public void Copy(AuthorsW other)
        {
            this.GetType().GetProperties().ToList().ForEach(property => property.SetValue(this, property.GetValue(other)));
        }

        /// <summary>
        /// It converts AuthorsW to Authors.
        /// </summary>
        /// <returns>It returns a db compatible entity.</returns>
        public Authors Convert()
        {
            Authors au = new Authors();
            au.BirthCountry = BirthCountry;
            au.BirthName = BirthName;
            au.BirthYear = BirthYear;
            au.Name = Name;
            au.Nationality = Nationality;
            return au;
        }
    }
}
