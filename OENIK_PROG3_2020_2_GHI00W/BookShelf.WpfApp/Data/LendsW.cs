﻿namespace BookShelf.WpfApp.Data
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShelf.Data.Entities;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// The lend class in WPF App.
    /// </summary>
    public class LendsW : ObservableObject
    {
        private int lendId;
        private int personId;
        private int bookId;
        private DateTime lendDate;
        private DateTime givebackDate;

        /// <summary>
        /// Gets or sets AuthorId is a primary key in the Authors table.
        /// </summary>
        public int LendId
        {
            get { return lendId; }
            set { Set(ref lendId, value); }
        }

        /// <summary>
        /// Gets or sets Pple is a reference for People which has lend a book.
        /// </summary>
        public int PersonId
        {
            get { return personId; }
            set { Set(ref personId, value); }
        }

        /// <summary>
        /// Gets or sets Bok is a reference for Books which is lend.
        /// </summary>
        public int BookId
        {
            get { return bookId; }
            set { Set(ref bookId, value); }
        }

        /// <summary>
        /// Gets or sets LendDate is an attribute in the Lends table. It signs the date of lending.
        /// </summary>
        public DateTime LendDate
        {
            get { return lendDate; }
            set { Set(ref lendDate, value); }
        }

        /// <summary>
        /// Gets or sets GivebackDate is an attribute in the Lends table. It signs the date of giving back.
        /// </summary>
        public DateTime GivebackDate
        {
            get { return givebackDate; }
            set { Set(ref givebackDate, value); }
        }

        /// <summary>
        /// It makes a copy about the other object.
        /// </summary>
        /// <param name="other">The object that should be copied.</param>
        public void Copy(LendsW other)
        {
            this.GetType().GetProperties().ToList().ForEach(property => property.SetValue(this, property.GetValue(other)));
        }

        /// <summary>
        /// It converts LendsW to Lends.
        /// </summary>
        /// <returns>It returns a db compatible entity.</returns>
        public Lends Convert()
        {
            Lends lend = new Lends();
            lend.PersonId = PersonId;
            lend.BookId = BookId;
            lend.LendDate = LendDate;
            lend.GivebackDate = GivebackDate;
            return lend;
        }

        /// <summary>
        /// Returns a string about the lend.
        /// </summary>
        /// <returns>The string about the lend.</returns>
        public override string ToString()
        {
            if (GivebackDate.Year < 1000)
            {
                return $"LendId: {LendId}. PersonId: {PersonId}. BookId: {BookId}. LendDate: {LendDate.ToString("yyyy/MM/dd", new CultureInfo("en-US"))}.";
            }
            else
            {
                return $"LendId: {LendId}. PersonId: {PersonId}. BookId: {BookId}. LendDate: {LendDate.ToString("yyyy/MM/dd", new CultureInfo("en-US"))}. GivebackDate: {GivebackDate.ToString("yyyy/MM/dd", new CultureInfo("en-US"))}.";
            }
        }
    }
}
