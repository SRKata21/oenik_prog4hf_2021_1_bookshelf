﻿namespace BookShelf.WpfApp.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShelf.Data.Entities;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// The book class in WPF App.
    /// </summary>
    public class BooksW : ObservableObject
    {
        private int bookId;
        private string title;
        private string publisher;
        private string place;
        private int year;
        private int pages;
        private DateTime getDate;
        private int authorId;

        /// <summary>
        /// Gets or sets BookId is a primary key in the Books table.
        /// </summary>
        public int BookId
        {
            get { return bookId; }
            set { Set(ref bookId, value); }
        }

        /// <summary>
        /// Gets or sets Title is an attribute key in the Books table.
        /// </summary>
        public string Title
        {
            get { return title; }
            set { Set(ref title, value); }
        }

        /// <summary>
        /// Gets or sets Publisher is an attribute in the Books table.
        /// </summary>
        public string Publisher
        {
            get { return publisher; }
            set { Set(ref publisher, value); }
        }

        /// <summary>
        /// Gets or sets Place is an attribute in the Books table. The place where the book was published.
        /// </summary>
        public string Place
        {
            get { return place; }
            set { Set(ref place, value); }
        }

        /// <summary>
        /// Gets or sets Year is an attribute in the Books table. The year when the book was published.
        /// </summary>
        public int Year
        {
            get { return year; }
            set { Set(ref year, value); }
        }

        /// <summary>
        /// Gets or sets Pages is an attribute in the Books table.
        /// </summary>
        public int Pages
        {
            get { return pages; }
            set { Set(ref pages, value); }
        }

        /// <summary>
        /// Gets or sets LendDate is an attribute in the Books table. It signs the date of getting.
        /// </summary>
        public DateTime GetDate
        {
            get { return getDate; }
            set { Set(ref getDate, value); }
        }

        /// <summary>
        /// Gets or sets Pple is a reference for People which has lend a book.
        /// </summary>
        public int AuthorId
        {
            get { return authorId; }
            set { Set(ref authorId, value); }
        }

        /// <summary>
        /// It makes a copy about the other object.
        /// </summary>
        /// <param name="other">The object that should be copied.</param>
        public void Copy(BooksW other)
        {
            this.GetType().GetProperties().ToList().ForEach(property => property.SetValue(this, property.GetValue(other)));
        }

        /// <summary>
        /// It converts BooksW to Books.
        /// </summary>
        /// <returns>It returns a db compatible entity.</returns>
        public Books Convert()
        {
            Books book = new Books();
            book.Title = Title;
            book.AuthorId = AuthorId;
            book.Publisher = Publisher;
            book.Place = Place;
            book.Year = Year;
            book.Pages = Pages;
            book.GetDate = GetDate;
            return book;
        }
    }
}
