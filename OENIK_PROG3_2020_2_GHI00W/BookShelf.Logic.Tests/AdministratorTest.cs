﻿namespace BookShelf.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text;
    using BookShelf.Data.Entities;
    using BookShelf.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// AdministartorTest in .Logic.Tests. to check the AdministratorUser logic.
    /// </summary>
    [TestFixture]
    public class AdministratorTest
    {
        /// <summary>
        /// It tests wheather works correct or not the inserter in the author table.
        /// </summary>
        [Test]
        public void TestInsertAuthor()
        {
            Mock<IBookManager> mockBookRepo = new Mock<IBookManager>(MockBehavior.Loose);
            Mock<IAuthorManager> mockAuthorRepo = new Mock<IAuthorManager>(MockBehavior.Loose);
            Mock<IPeopleManager> mockPeopleRepo = new Mock<IPeopleManager>(MockBehavior.Loose);
            Mock<ILendManager> mockLendRepo = new Mock<ILendManager>(MockBehavior.Loose);

            mockAuthorRepo.Setup(inserter => inserter.Insert(It.IsAny<Authors>()));

            AdministratorUser admin = new AdministratorUser(mockBookRepo.Object, mockAuthorRepo.Object, mockPeopleRepo.Object, mockLendRepo.Object);
            Authors aut = new Authors { AuthorId = 78, BirthCountry = "England", BirthName = "John Ronald Reuel Tolkien", BirthYear = 1892, Name = "J.R.R.Tolkien", Nationality = "british" };
            admin.InsertAuthor(aut);

            mockAuthorRepo.Verify(inserter => inserter.Insert(aut), Times.Once);
        }

        /// <summary>
        /// It tests wheather works correct or not the inserter in the author table.
        /// </summary>
        [Test]
        public void TestGetOnePerson()
        {
            Mock<IBookManager> mockBookRepo = new Mock<IBookManager>();
            Mock<IAuthorManager> mockAuthorRepo = new Mock<IAuthorManager>();
            Mock<IPeopleManager> mockPeopleRepo = new Mock<IPeopleManager>();
            Mock<ILendManager> mockLendRepo = new Mock<ILendManager>();
            List<People> person = new List<People>
            {
                new People() { PersonId = 1, Address = "Budapest IX. Rákos utca 3", BirthPlace = "Budapest", MotherName = "Szegény Ibolya", Name = "Nemecsek Ernő", Email = "nemecsekerno@kisbetu-grund.hu", EnterDate = Convert.ToDateTime("2005.03.16", CultureInfo.CurrentCulture), BirthDate = Convert.ToDateTime("1995.03.16", CultureInfo.CurrentCulture) },
                new People() { PersonId = 2, Address = "Debrecen, Kollégium utca 13, 133 szoba", BirthPlace = "Sárospatak", MotherName = "Szorgos Ilona", Name = "Nyilas Misi", Email = "nyilasm1892@koll-debrecen.hu", EnterDate = Convert.ToDateTime("2002.07.26", CultureInfo.CurrentCulture), BirthDate = Convert.ToDateTime("1987.11.16", CultureInfo.CurrentCulture) },
                new People() { PersonId = 3, Address = "KisDöbrögfalva 3333", BirthPlace = "Döbrög", MotherName = "Lúdas Mami", Name = "Lúdas Matyi", Email = "ludas_matyko@haromszor.hu", EnterDate = Convert.ToDateTime("2015.08.01", CultureInfo.CurrentCulture), BirthDate = Convert.ToDateTime("2000.12.31", CultureInfo.CurrentCulture) },
            };
            List<People> expectedBooks = new List<People>() { person[0], person[1], person[2] };

            mockPeopleRepo.Setup(getter => getter.GetOne(1)).Returns(person[0]);

            AdministratorUser admin = new AdministratorUser(mockBookRepo.Object, mockAuthorRepo.Object, mockPeopleRepo.Object, mockLendRepo.Object);
            var result = admin.GetOnePerson(1);

            mockPeopleRepo.Verify(getter => getter.GetOne(1), Times.Once);
        }

        /// <summary>
        /// It tests wheather works correct or not the inserter in the author table.
        /// </summary>
        [Test]
        public void TestDeleteBook()
        {
            Mock<IBookManager> mockBookRepo = new Mock<IBookManager>(MockBehavior.Loose);
            Mock<IAuthorManager> mockAuthorRepo = new Mock<IAuthorManager>(MockBehavior.Loose);
            Mock<IPeopleManager> mockPeopleRepo = new Mock<IPeopleManager>(MockBehavior.Loose);
            Mock<ILendManager> mockLendRepo = new Mock<ILendManager>(MockBehavior.Loose);

            mockBookRepo.Setup(deleter => deleter.Remove(It.IsAny<int>()));

            AdministratorUser admin = new AdministratorUser(mockBookRepo.Object, mockAuthorRepo.Object, mockPeopleRepo.Object, mockLendRepo.Object);
            admin.RemoveBookByKey(1);

            mockBookRepo.Verify(deleter => deleter.Remove(1), Times.Once);
        }
    }
}
