﻿namespace BookShelf.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using BookShelf.Data.Entities;
    using BookShelf.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// ReaderTest in .Logic.Tests. to check the ReaderUser logic.
    /// </summary>
    [TestFixture]
    public class ReaderTest
    {
        /// <summary>
        /// It tests wheather works correct or not the all record getter.
        /// </summary>
        [Test]
        public void TestGetAllBooks()
        {
            Mock<IBookManager> mockReaderRepo = new Mock<IBookManager>(MockBehavior.Loose);
            List<Books> books = new List<Books>
            {
                new Books() { BookId = 1, AuthorId = 2, GetDate = Convert.ToDateTime("2006.03.16", CultureInfo.CurrentCulture), Pages = 278, Place = "Budapest", Publisher = "Európa", Title = "Macska a galambok között", Year = 2005 },
                new Books() { BookId = 2, AuthorId = 2, GetDate = Convert.ToDateTime("2015.03.06", CultureInfo.CurrentCulture), Pages = 240, Place = "Budapest", Publisher = "Európa", Title = "A nagy négyes", Year = 2013 },
                new Books() { BookId = 3, AuthorId = 3, GetDate = Convert.ToDateTime("2017.07.16", CultureInfo.CurrentCulture), Pages = 574, Place = "Budapest", Publisher = "GABO", Title = "Átmeneti üresedés", Year = 2012 },
                new Books() { BookId = 4, AuthorId = 3, GetDate = Convert.ToDateTime("2002.03.16", CultureInfo.CurrentCulture), Pages = 674, Place = "Budapest", Publisher = "Animus", Title = "Harry Potter és a tűz serlege", Year = 2000 },
                new Books() { BookId = 5, AuthorId = 4, GetDate = Convert.ToDateTime("2016.07.16", CultureInfo.CurrentCulture), Pages = 182, Place = "Déva", Publisher = "Corvin", Title = "Vuk", Year = 2002 },
                new Books() { BookId = 6, AuthorId = 5, GetDate = Convert.ToDateTime("2018.07.16", CultureInfo.CurrentCulture), Pages = 250, Place = "Marosvásárhely", Publisher = "Móra", Title = "Rejtőző kavicsok I", Year = 2012 },
                new Books() { BookId = 7, AuthorId = 4, GetDate = Convert.ToDateTime("2015.07.16", CultureInfo.CurrentCulture), Pages = 182, Place = "Déva", Publisher = "Corvin", Title = "Bogáncs", Year = 2002 },
            };
            List<Books> expectedBooks = new List<Books>() { books[0], books[1], books[2], books[3], books[4], books[5], books[6] };
            mockReaderRepo.Setup(getter => getter.GetAll()).Returns(books.AsQueryable());

            Mock<IAuthorManager> mockAuthorRepo = new Mock<IAuthorManager>(MockBehavior.Loose);
            Mock<IPeopleManager> mockPeopleRepo = new Mock<IPeopleManager>(MockBehavior.Loose);
            Mock<ILendManager> mockLendRepo = new Mock<ILendManager>(MockBehavior.Loose);

            ReaderUser readerLogic = new ReaderUser(mockReaderRepo.Object, mockAuthorRepo.Object, mockPeopleRepo.Object, mockLendRepo.Object);
            var result = readerLogic.GetAllBooks();

            mockReaderRepo.Verify(getter => getter.GetAll(), Times.Once);
            mockReaderRepo.Verify(getter => getter.GetOne(It.IsAny<int>()), Times.Never);
        }

        /// <summary>
        /// It tests wheather works correct or not the famous selector record getter.
        /// </summary>
        [Test]
        public void TestGetFamousBooks()
        {
            var logic = LogicCreator.CreateReaderLogicWithMocks();
            var famousBook = logic.GetFamousBooks();

            Assert.That(famousBook, Is.EquivalentTo(LogicCreator.ExpectedFamBooks));
            LogicCreator.MockPeopleRepo.Verify(peop => peop.GetAll(), Times.Never);
            LogicCreator.MockLendRepo.Verify(lnd => lnd.GetAll(), Times.Exactly(2));
            LogicCreator.MockBookRepo.Verify(bk => bk.GetAll(), Times.Once);
            LogicCreator.MockAuthorRepo.Verify(athr => athr.GetAll(), Times.Once);
        }

        /// <summary>
        /// It tests wheather works correct or not the hungarian selector record getter.
        /// </summary>
        [Test]
        public void TestGetHungarianBooks()
        {
            var logic = LogicCreator.CreateReaderLogicWithMocks();
            var hunBook = logic.GetBookHungarian();

            Assert.That(hunBook, Is.EquivalentTo(LogicCreator.ExpectedHunBooks));
            LogicCreator.MockPeopleRepo.Verify(peop => peop.GetAll(), Times.Never);
            LogicCreator.MockLendRepo.Verify(lnd => lnd.GetAll(), Times.Never);
            LogicCreator.MockBookRepo.Verify(bk => bk.GetAll(), Times.Once);
            LogicCreator.MockAuthorRepo.Verify(athr => athr.GetAll(), Times.Once);
        }
    }
}
