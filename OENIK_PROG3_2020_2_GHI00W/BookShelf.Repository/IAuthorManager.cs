﻿namespace BookShelf.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BookShelf.Data.Entities;

    /// <summary>
    /// The descended interface of IRepository. The update is written here.
    /// </summary>
    public interface IAuthorManager : IManager<Authors>
    {
        /// <summary>
        /// It changes the name of the author.
        /// </summary>
        /// <param name="id">The id of the author.</param>
        /// <param name="name">The new name of the author.</param>
        void ChangeName(int id, string name);

        /// <summary>
        /// It changes the birthname of the author.
        /// </summary>
        /// <param name="id">The id of the author.</param>
        /// <param name="name">The new birthname of the author.</param>
        void ChangeBirthName(int id, string name);

        /// <summary>
        /// It changes the country of the author.
        /// </summary>
        /// <param name="id">The id of the author.</param>
        /// <param name="country">The new country of the author.</param>
        void ChangeCountry(int id, string country);

        /// <summary>
        /// It changes the nationality of the author.
        /// </summary>
        /// <param name="id">The id of the author.</param>
        /// <param name="nationality">The new nationality of the author.</param>
        void ChangeNationality(int id, string nationality);

        /// <summary>
        /// It changes the birth year of the author.
        /// </summary>
        /// <param name="id">The id of the author.</param>
        /// <param name="year">The new birth year of the author.</param>
        void ChangeYear(int id, int year);
    }
}
