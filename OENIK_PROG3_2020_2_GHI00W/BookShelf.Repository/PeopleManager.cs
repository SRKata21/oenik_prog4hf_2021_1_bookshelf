﻿namespace BookShelf.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BookShelf.Data.Entities;
    using Castle.Core.Internal;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The descended interface of IRepository. The update is written here.
    /// </summary>
    public class PeopleManager : Manager<People>, IPeopleManager
    {
        /// <summary>
        /// dbctx is the Database reference.
        /// </summary>
        private BookShelfDBContext dbctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="PeopleManager"/> class.
        /// </summary>
        /// <param name="ctx">The given DbContext for the initialization.</param>
        public PeopleManager(BookShelfDBContext ctx)
            : base(ctx)
        {
            this.dbctx = ctx;
        }

        /// <summary>
        /// It changes the address of the person.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="address">The new the address of the person.</param>
        /// <returns>Whether change is successed or not.</returns>
        public bool ChangeAddress(int id, string address)
        {
            People person = this.GetAll().SingleOrDefault(pers => pers.PersonId == id);
            if (person == null)
            {
                return false;
            }

            person.Address = address;
            this.dbctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// It changes the date of birth of the person.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="bdate">The new date of birth of the person.</param>
        public void ChangeBirthDate(int id, DateTime bdate)
        {
            People person = this.GetAll().SingleOrDefault(pers => pers.PersonId == id);
            person.BirthDate = bdate;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the place where the person was born.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="place">The new birthplace where the person was born.</param>
        public void ChangeBirthPlace(int id, string place)
        {
            People person = this.GetAll().SingleOrDefault(pers => pers.PersonId == id);
            person.BirthPlace = place;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the email of the person.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="email">The new the email of the person.</param>
        /// <returns>Whether change is successed or not.</returns>
        public bool ChangeEmail(int id, string email)
        {
            People person = this.GetAll().SingleOrDefault(pers => pers.PersonId == id);
            if (person == null)
            {
                return false;
            }

            person.Email = email;
            this.dbctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// It changes the date of entering of person at library.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="edate">The new date of entering at library.</param>
        public void ChangeEnterDate(int id, DateTime edate)
        {
            People person = this.GetAll().SingleOrDefault(pers => pers.PersonId == id);
            person.EnterDate = edate;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the name of the mother the person.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="name">The new name of the mother of the person.</param>
        public void ChangeMotherName(int id, string name)
        {
            People person = this.GetAll().SingleOrDefault(pers => pers.PersonId == id);
            person.MotherName = name;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the name of the person.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="name">The new name of the person.</param>
        public void ChangeName(int id, string name)
        {
            People person = this.GetAll().SingleOrDefault(pers => pers.PersonId == id);
            person.Name = name;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public override People GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(pers => pers.PersonId == id);
        }
    }
}
