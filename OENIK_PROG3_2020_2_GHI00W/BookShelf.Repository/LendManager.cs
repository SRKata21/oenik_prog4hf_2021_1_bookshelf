﻿namespace BookShelf.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BookShelf.Data.Entities;
    using Castle.Core.Internal;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The descended interface of IRepository. The update is written here.
    /// </summary>
    public class LendManager : Manager<Lends>, ILendManager
    {
        /// <summary>
        /// dbctx is the Database reference.
        /// </summary>
        private BookShelfDBContext dbctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="LendManager"/> class.
        /// </summary>
        /// <param name="ctx">The given DbContext for the initialization.</param>
        public LendManager(BookShelfDBContext ctx)
            : base(ctx)
        {
            this.dbctx = ctx;
        }

        /// <summary>
        /// It changes the bookid of the lending.
        /// </summary>
        /// <param name="id">The id of the lending.</param>
        /// <param name="newBookId">The new bookid of the lending.</param>
        public void ChangeBook(int id, int newBookId)
        {
            Lends lend = this.GetAll().SingleOrDefault(lnd => lnd.LendId == id);
            lend.BookId = newBookId;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the date of the giving back.
        /// </summary>
        /// <param name="id">The id of the lending.</param>
        /// <param name="gbdate">The new date of the giving back.</param>
        public void ChangeGiveBackDate(int id, DateTime gbdate)
        {
            Lends lend = this.GetAll().SingleOrDefault(lnd => lnd.LendId == id);
            lend.GivebackDate = gbdate;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the date of the lending.
        /// </summary>
        /// <param name="id">The id of the lending.</param>
        /// <param name="ldate">The new date of the lending.</param>
        public void ChangeLendDate(int id, DateTime ldate)
        {
            Lends lend = this.GetAll().SingleOrDefault(lnd => lnd.LendId == id);
            lend.LendDate = ldate;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the personid of the lending.
        /// </summary>
        /// <param name="id">The bookid of the lending.</param>
        /// <param name="personId">The new personid of the lending.</param>
        public void ChangePerson(int id, int personId)
        {
            Lends lend = this.GetAll().SingleOrDefault(lnd => lnd.LendId == id);
            lend.PersonId = personId;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public override Lends GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(lnd => lnd.LendId == id);
        }
    }
}
