﻿namespace BookShelf.Repository
{
    using System;
    using System.Linq;

    /// <summary>
    /// IRepository interface in .Repository for CR(U)D operations. It's the ancient interface.
    /// </summary>
    /// <typeparam name="T">T is a generic type for entities.</typeparam>
    public interface IManager<T>
        where T : class
    {
        // CRD (create, read, delete) methodes without U(pdate)

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        T GetOne(int id);

        /// <summary>
        /// It returns all existing objects with T type.
        /// </summary>
        /// <returns>A list of all existing objects. It's IQueriable so I can make queries using these.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// It makes the inserting in the tables of database.
        /// </summary>
        /// <param name="entity">The methode gets the entity to insert in the database.</param>
        void Insert(T entity);

        /// <summary>
        /// It removes the entity from the database.
        /// </summary>
        /// <param name="entity">The entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        bool Remove(T entity);

        /// <summary>
        /// It removes the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        bool Remove(int id);
    }
}
