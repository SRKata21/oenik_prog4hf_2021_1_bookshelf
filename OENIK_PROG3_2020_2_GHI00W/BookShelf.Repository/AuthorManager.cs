﻿namespace BookShelf.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BookShelf.Data.Entities;
    using Castle.Core.Internal;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Concret repository class for implementing CRUD methodes.
    /// </summary>
    public class AuthorManager : Manager<Authors>, IAuthorManager
    {
        /// <summary>
        /// dbctx is the Database reference.
        /// </summary>
        private BookShelfDBContext dbctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorManager"/> class.
        /// </summary>
        /// <param name="ctx">The given DbContext for the initialization.</param>
        public AuthorManager(BookShelfDBContext ctx)
            : base(ctx)
        {
            this.dbctx = ctx;
        }

        /// <summary>
        /// It changes the birthname of the author.
        /// </summary>
        /// <param name="id">The id of the author.</param>
        /// <param name="name">The new birthname of the author.</param>
        public void ChangeBirthName(int id, string name)
        {
            Authors author = this.GetAll().SingleOrDefault(autr => autr.AuthorId == id);
            author.Name = name;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the country of the author.
        /// </summary>
        /// <param name="id">The id of the author.</param>
        /// <param name="country">The new country of the author.</param>
        public void ChangeCountry(int id, string country)
        {
            Authors author = this.GetAll().SingleOrDefault(autr => autr.AuthorId == id);
            author.BirthCountry = country;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the name of the author.
        /// </summary>
        /// <param name="id">The id of the author.</param>
        /// <param name="name">The new name of the author.</param>
        public void ChangeName(int id, string name)
        {
            Authors author = this.GetAll().SingleOrDefault(autr => autr.AuthorId == id);
            author.Name = name;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the nationality of the author.
        /// </summary>
        /// <param name="id">The id of the author.</param>
        /// <param name="nationality">The new nationality of the author.</param>
        public void ChangeNationality(int id, string nationality)
        {
            Authors author = this.GetAll().SingleOrDefault(autr => autr.AuthorId == id);
            author.Nationality = nationality;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the birth year of the author.
        /// </summary>
        /// <param name="id">The id of the author.</param>
        /// <param name="year">The new birth year of the author.</param>
        public void ChangeYear(int id, int year)
        {
            Authors author = this.GetAll().SingleOrDefault(autr => autr.AuthorId == id);
            author.BirthYear = year;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public override Authors GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(autr => autr.AuthorId == id);
        }
    }
}
