﻿namespace BookShelf.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BookShelf.Data.Entities;

    /// <summary>
    /// The descended interface of IRepository. The update is written here.
    /// </summary>
    public interface IPeopleManager : IManager<People>
    {
        /// <summary>
        /// It changes the name of the person.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="name">The new name of the person.</param>
        void ChangeName(int id, string name);

        /// <summary>
        /// It changes the name of the mother the person.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="name">The new name of the mother of the person.</param>
        void ChangeMotherName(int id, string name);

        /// <summary>
        /// It changes the place where the person was born.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="place">The new birthplace where the person was born.</param>
        void ChangeBirthPlace(int id, string place);

        /// <summary>
        /// It changes the address of the person.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="address">The new the address of the person.</param>
        /// <returns>Whether change is successed or not.</returns>
        bool ChangeAddress(int id, string address);

        /// <summary>
        /// It changes the email of the person.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="email">The new the email of the person.</param>
        /// <returns>Whether change is successed or not.</returns>
        bool ChangeEmail(int id, string email);

        /// <summary>
        /// It changes the date of entering of person at library.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="edate">The new date of entering at library.</param>
        void ChangeEnterDate(int id, DateTime edate);

        /// <summary>
        /// It changes the date of birth of the person.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="bdate">The new date of birth of the person.</param>
        void ChangeBirthDate(int id, DateTime bdate);
    }
}
