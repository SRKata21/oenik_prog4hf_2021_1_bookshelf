﻿namespace BookShelf.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BookShelf.Data.Entities;
    using Castle.Core.Internal;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Concret repository class for implementing CRUD methodes.
    /// </summary>
    public class BookManager : Manager<Books>, IBookManager
    {
        /// <summary>
        /// dbctx is the Database reference.
        /// </summary>
        private BookShelfDBContext dbctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="BookManager"/> class.
        /// </summary>
        /// <param name="ctx">The given DbContext for the initialization.</param>
        public BookManager(BookShelfDBContext ctx)
            : base(ctx)
        {
            this.dbctx = ctx;
        }

        /// <summary>
        /// It changes the authorid of the book.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="authorId">The new id of the author.</param>
        public void ChangeAuthor(int id, int authorId)
        {
            Books book = this.GetAll().SingleOrDefault(bok => bok.BookId == id);
            book.AuthorId = authorId;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the date of getting book in library.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="gdate">The date of getting book.</param>
        public void ChangeGetDate(int id, DateTime gdate)
        {
            Books book = this.GetAll().SingleOrDefault(bok => bok.BookId == id);
            book.GetDate = gdate;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the page of the book.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="page">The pafe of the book.</param>
        public void ChangePage(int id, int page)
        {
            Books book = this.GetAll().SingleOrDefault(bok => bok.BookId == id);
            book.Pages = page;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the place where the book was published.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="place">The new place where the book was published.</param>
        public void ChangePlace(int id, string place)
        {
            Books book = this.GetAll().SingleOrDefault(bok => bok.BookId == id);
            book.Place = place;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the publisher of the book.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="publisher">The new publisher of the book.</param>
        public void ChangePublisher(int id, string publisher)
        {
            Books book = this.GetAll().SingleOrDefault(bok => bok.BookId == id);
            book.Publisher = publisher;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the title of the book.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="title">The new title of the book.</param>
        public void ChangeTitle(int id, string title)
        {
            Books book = this.GetAll().SingleOrDefault(bok => bok.BookId == id);
            book.Title = title;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It changes the year when the book was published.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="year">The new year when the book was published.</param>
        public void ChangeYear(int id, int year)
        {
            Books book = this.GetAll().SingleOrDefault(bok => bok.BookId == id);
            book.Year = year;
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public override Books GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(bok => bok.BookId == id);
        }
    }
}
