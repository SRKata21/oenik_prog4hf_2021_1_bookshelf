﻿namespace BookShelf.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BookShelf.Data.Entities;
    using BookShelf.Repository;

    /// <summary>
    /// This class helps the administrator in adding and updating dates.
    /// </summary>
    public class AdministratorUser : IAdministratorUser
    {
        private IBookManager bookRepos;
        private IAuthorManager authorRepos;
        private IPeopleManager personRepos;
        private ILendManager lendRepos;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdministratorUser"/> class.
        /// </summary>
        /// <param name="bookRepos">The book repository.</param>
        /// <param name="authorRepos">The author repository.</param>
        /// <param name="personRepos">The person repository.</param>
        /// <param name="lendRepos">The lending repository.</param>
        public AdministratorUser(IBookManager bookRepos, IAuthorManager authorRepos, IPeopleManager personRepos, ILendManager lendRepos)
        {
            this.bookRepos = bookRepos;
            this.authorRepos = authorRepos;
            this.personRepos = personRepos;
            this.lendRepos = lendRepos;
        }

        /// <summary>
        /// It searches all entities by key from the database.
        /// </summary>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public IList<People> GetAllPeople()
        {
            return this.personRepos.GetAll().ToList<People>();
        }

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public People GetOnePerson(int id)
        {
            return this.personRepos.GetOne(id);
        }

        /// <summary>
        /// It makes the inserting in the tables of database.
        /// </summary>
        /// <param name="entity">The methode gets the entity to insert in the database.</param>
        public void InsertAuthor(Authors entity)
        {
            this.authorRepos.Insert(entity);
        }

        /// <summary>
        /// It makes the inserting in the tables of database.
        /// </summary>
        /// <param name="entity">The methode gets the entity to insert in the database.</param>
        public void InsertBook(Books entity)
        {
            this.bookRepos.Insert(entity);
        }

        /// <summary>
        /// It makes the inserting in the tables of database.
        /// </summary>
        /// <param name="entity">The methode gets the entity to insert in the database.</param>
        public void InsertPerson(People entity)
        {
            this.personRepos.Insert(entity);
        }

        /// <summary>
        /// It removes the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public bool RemoveAuthorByKey(int id)
        {
            if (id == 1)
            {
                return false;
            }

            List<int> removables = (from bookRow in this.bookRepos.GetAll()
                                    where bookRow.AuthorId == id
                                    select bookRow.BookId).ToList();

            foreach (int item in removables)
            {
                this.bookRepos.ChangeAuthor(item, 0);
            }

            return this.authorRepos.Remove(id);
        }

        /// <summary>
        /// It removes the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public bool RemoveBookByKey(int id)
        {
            List<int> removables = (from lendRow in this.lendRepos.GetAll()
                                    where lendRow.BookId == id
                                    select lendRow.LendId).ToList();

            foreach (int item in removables)
            {
                this.lendRepos.Remove(item);
            }

            return this.bookRepos.Remove(id);
        }

        /// <summary>
        /// It removes the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public bool RemovePersonByKey(int id)
        {
            List<int> removables = (from lendRow in this.lendRepos.GetAll()
                                   where lendRow.PersonId == id
                                   select lendRow.LendId).ToList();

            foreach (int item in removables)
            {
                this.lendRepos.Remove(item);
            }

            return this.personRepos.Remove(id);
        }
    }
}
