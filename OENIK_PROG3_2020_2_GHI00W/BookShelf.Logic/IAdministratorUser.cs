﻿namespace BookShelf.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BookShelf.Data.Entities;

    /// <summary>
    /// This interface helps the administrator in adding and updating dates.
    /// </summary>
    public interface IAdministratorUser
    {
        /// <summary>
        /// It searches all entities by key from the database.
        /// </summary>
        /// <returns>Returns if the operating was succeded or not.</returns>
        IList<People> GetAllPeople();

        /// <summary>
        /// It makes the inserting in the tables of database.
        /// </summary>
        /// <param name="entity">The methode gets the entity to insert in the database.</param>
        void InsertAuthor(Authors entity);

        /// <summary>
        /// It makes the inserting in the tables of database.
        /// </summary>
        /// <param name="entity">The methode gets the entity to insert in the database.</param>
        void InsertBook(Books entity);

        /// <summary>
        /// It makes the inserting in the tables of database.
        /// </summary>
        /// <param name="entity">The methode gets the entity to insert in the database.</param>
        void InsertPerson(People entity);

        /// <summary>
        /// It removes the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        bool RemovePersonByKey(int id);

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        People GetOnePerson(int id);

        /// <summary>
        /// It removes the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        bool RemoveBookByKey(int id);

        /// <summary>
        /// It removes the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        bool RemoveAuthorByKey(int id);
    }
}
