﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "I don't have to write copyright.", Scope = "namespace", Target = "~N:BookShelf.Logic")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "I wouldn't write XML header and copyright for Logic.Test_GolbalSuppressions.cs.")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "no this", Scope = "member", Target = "~M:BookShelf.Logic.LibrarianUser.TaskGetLendByReader(System.Int32)~System.Collections.Generic.IList{BookShelf.Logic.LendingByReader}")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "no this", Scope = "member", Target = "~M:BookShelf.Logic.ReaderUser.TaskGetBookHungarian~System.Collections.Generic.IList{BookShelf.Logic.BookDatas}")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "no this", Scope = "member", Target = "~M:BookShelf.Logic.ReaderUser.TaskGetFamBook~System.Collections.Generic.IList{BookShelf.Logic.BookDatas}")]
