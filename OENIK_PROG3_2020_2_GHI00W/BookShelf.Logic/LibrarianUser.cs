﻿namespace BookShelf.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShelf.Data.Entities;
    using BookShelf.Repository;

    /// <summary>
    /// This interface helps the librarian in search.
    /// </summary>
    public class LibrarianUser : ILibrarianUser
    {
        private IBookManager bookRepos;
        private IAuthorManager authorRepos;
        private IPeopleManager personRepos;
        private ILendManager lendRepos;

        /// <summary>
        /// Initializes a new instance of the <see cref="LibrarianUser"/> class.
        /// </summary>
        /// <param name="bookRepos">The book repository.</param>
        /// <param name="authorRepos">The author repository.</param>
        /// <param name="personRepos">The person repository.</param>
        /// <param name="lendRepos">The lending repository.</param>
        public LibrarianUser(IBookManager bookRepos, IAuthorManager authorRepos, IPeopleManager personRepos, ILendManager lendRepos)
        {
            this.bookRepos = bookRepos;
            this.authorRepos = authorRepos;
            this.personRepos = personRepos;
            this.lendRepos = lendRepos;
        }

        /// <summary>
        /// It changes the name of the author.
        /// </summary>
        /// <param name="id">The id of the author.</param>
        /// <param name="name">The new name of the author.</param>
        public void ChangeAuthorName(int id, string name)
        {
            this.authorRepos.ChangeName(id, name);
        }

        /// <summary>
        /// It changes the title of the book.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="y">The correct year of publishing the book.</param>
        public void ChangeBookYear(int id, int y)
        {
            this.bookRepos.ChangeYear(id, y);
        }

        /// <summary>
        /// It changes the date of the giving back.
        /// </summary>
        /// <param name="id">The bookid of the lending.</param>
        /// <param name="gbdate">The new date of the giving back.</param>
        public void ChangeLendGiveBackDate(int id, DateTime gbdate)
        {
            this.lendRepos.ChangeGiveBackDate(id, gbdate);
        }

        /// <summary>
        /// It changes the name of the person.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="name">The new name of the person.</param>
        public void ChangePersonName(int id, string name)
        {
            this.personRepos.ChangeName(id, name);
        }

        /// <summary>
        /// It searches all entities by key from the database.
        /// </summary>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public IList<Lends> GetAllLends()
        {
            return this.lendRepos.GetAll().ToList<Lends>();
        }

        /// <summary>
        /// It searches the dates of lendings by reader.
        /// </summary>
        /// <param name="idReader">It is the id of the reader.</param>
        /// <returns>The list lendings of the reader.</returns>
        public IList<LendingByReader> GetLendByReader(int idReader)
        {
            var datas = from reader in this.personRepos.GetAll()
                        join lend in this.lendRepos.GetAll() on reader.PersonId equals lend.PersonId
                        join book in this.bookRepos.GetAll() on lend.BookId equals book.BookId
                        join author in this.authorRepos.GetAll() on book.AuthorId equals author.AuthorId
                        where reader.PersonId == idReader
                        orderby lend.LendDate
                        select new LendingByReader()
                        {
                            Title = book.Title,
                            AuthorName = author.Name,
                            ReaderName = reader.Name,
                            LendDate = lend.LendDate,
                            GiveBackDate = lend.GivebackDate,
                        };

            return datas.ToList();
        }

        /// <summary>
        /// It searches the dates of lendings by reader.
        /// </summary>
        /// <param name="id">It is the id of the reader.</param>
        /// <returns>The list lendings of the reader.</returns>
        public IList<LendingByReader> TaskGetLendByReader(int id)
        {
            return Task<IList<LendingByReader>>.Factory.StartNew(() => GetLendByReader(id)).Result;
        }

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public Authors GetOneAuthor(int id)
        {
            return this.authorRepos.GetOne(id);
        }

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public Books GetOneBook(int id)
        {
            return this.bookRepos.GetOne(id);
        }

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public Lends GetOneLend(int id)
        {
            return this.lendRepos.GetOne(id);
        }

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public People GetOnePerson(int id)
        {
            return this.personRepos.GetOne(id);
        }

        /// <summary>
        /// It makes the inserting in the tables of database.
        /// </summary>
        /// <param name="entity">The methode gets the entity to insert in the database.</param>
        public void InsertLend(Lends entity)
        {
            this.lendRepos.Insert(entity);
        }
    }
}
